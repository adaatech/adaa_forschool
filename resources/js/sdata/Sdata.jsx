import image1 from '../imgs/adaaForInvest/section_11/img-adaa-platform1.png';
import image2 from '../imgs/adaaForInvest/section_11/img-adaa-platform2.png';
import image3 from '../imgs/adaaForInvest/section_11/img-adaa-platform3.png';

import iconInvestS02_1 from '../imgs/adaaForInvest/section_02/icon-ambition-1.png';
import iconInvestS02_2 from '../imgs/adaaForInvest/section_02/icon-ambition-2.png';
import iconInvestS02_3 from '../imgs/adaaForInvest/section_02/icon-ambition-3.png';
import iconInvestS02_4 from '../imgs/adaaForInvest/section_02/icon-ambition-4.png';
import iconInvestS02_5 from '../imgs/adaaForInvest/section_02/icon-ambition-5.png';
import iconInvestS02_6 from '../imgs/adaaForInvest/section_02/icon-ambition-6.png';

import iconInvestS05_1 from '../imgs/adaaForInvest/section_05/icon1.png';
import iconInvestS05_2 from '../imgs/adaaForInvest/section_05/icon2.png';
import iconInvestS05_3 from '../imgs/adaaForInvest/section_05/icon3.png';
import iconInvestS05_4 from '../imgs/adaaForInvest/section_05/icon4.png';
import iconInvestS05_5 from '../imgs/adaaForInvest/section_05/icon5.png';
import iconInvestS05_6 from '../imgs/adaaForInvest/section_05/icon6.png';

import iconInvestS06_1 from '../imgs/adaaForInvest/section_06/001.jpg';
import iconInvestS06_2 from '../imgs/adaaForInvest/section_06/002.jpg';
import iconInvestS06_3 from '../imgs/adaaForInvest/section_06/003.jpg';
import iconInvestS06_4 from '../imgs/adaaForInvest/section_06/004.jpg';
import iconInvestS06_5 from '../imgs/adaaForInvest/section_06/005.jpg';
import iconInvestS06_6 from '../imgs/adaaForInvest/section_06/006.jpg';
import iconInvestS06_7 from '../imgs/adaaForInvest/section_06/007.jpg';
import iconInvestS06_8 from '../imgs/adaaForInvest/section_06/008.jpg';
import iconInvestS06_9 from '../imgs/adaaForInvest/section_06/009.jpg';
import iconInvestS06_10 from '../imgs/adaaForInvest/section_06/010.jpg';
import iconInvestS06_11 from '../imgs/adaaForInvest/section_06/011.jpg';
import iconInvestS06_12 from '../imgs/adaaForInvest/section_06/012.jpg';
import iconInvestS06_13 from '../imgs/adaaForInvest/section_06/013.jpg';
import iconInvestS06_14 from '../imgs/adaaForInvest/section_06/014.jpg';
import iconInvestS06_15 from '../imgs/adaaForInvest/section_06/015.jpg';
import iconInvestS06_16 from '../imgs/adaaForInvest/section_06/016.jpg';

import iconInvestS14_1 from '../imgs/adaaForInvest/section_14/Image1.png';
import iconInvestS14_2 from '../imgs/adaaForInvest/section_14/Image2.jpg';
import iconInvestS14_3 from '../imgs/adaaForInvest/section_14/Image3.png';
import iconInvestS14_4 from '../imgs/adaaForInvest/section_14/Image4.png';
import iconInvestS14_5 from '../imgs/adaaForInvest/section_14/Image5.png';
import iconInvestS14_6 from '../imgs/adaaForInvest/section_14/Image6.jpg';
import iconInvestS14_7 from '../imgs/adaaForInvest/section_14/Image7.png';
import iconInvestS14_8 from '../imgs/adaaForInvest/section_14/Image8.png';
import iconInvestS14_9 from '../imgs/adaaForInvest/section_14/Image9.png';

import iconInvestS16_1 from '../imgs/adaaForInvest/section_16/Image1.png';
import iconInvestS16_2 from '../imgs/adaaForInvest/section_16/Image2.png';
import iconInvestS16_3 from '../imgs/adaaForInvest/section_16/Image3.png';
import iconInvestS16_4 from '../imgs/adaaForInvest/section_16/Image4.png';
import iconInvestS16_5 from '../imgs/adaaForInvest/section_16/Image5.png';
import iconInvestS16_6 from '../imgs/adaaForInvest/section_16/Image6.png';
import iconInvestS16_7 from '../imgs/adaaForInvest/section_16/Image7.jpg';

import iconInvestS17_1 from '../imgs/adaaForInvest/section_17/001.png';
import iconInvestS17_2 from '../imgs/adaaForInvest/section_17/002.png';
import iconInvestS17_3 from '../imgs/adaaForInvest/section_17/003.png';
import iconInvestS17_4 from '../imgs/adaaForInvest/section_17/004.png';
import iconInvestS17_5 from '../imgs/adaaForInvest/section_17/005.png';
import iconInvestS17_6 from '../imgs/adaaForInvest/section_17/006.png';
import iconInvestS17_7 from '../imgs/adaaForInvest/section_17/007.png';
import iconInvestS17_8 from '../imgs/adaaForInvest/section_17/008.png';
import iconInvestS17_9 from '../imgs/adaaForInvest/section_17/009.png';


const Sdata = {
    adaaForSchoolS3Grid : [
        {
            id:"s31",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s32",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s33",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s34",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s31",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s32",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s33",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s34",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s31",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s32",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s33",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s34",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s31",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s32",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s33",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s34",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s31",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s32",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s33",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s34",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s31",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s32",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s33",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s34",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
        {
            id:"s3xx",
            imgsrc: icon5,
            title: "Possedez votre plateforme de e-learning"
        },
    ],
    adaaForInvestS2Grid : [
        {
            id:"P1",
            imgsrc: iconInvestS02_1,
            title: "285",
            content: ["Nouveaux parcours d’apprentissage (de plus de 20 heures chacun) à produire et rendre disponibles sur ", <br/>, <a href="https://www.adaalearning.com">www.adaalearning.com</a>],
        },
        {
            id:"P2",
            imgsrc: iconInvestS02_2,
            title: "10",
            content: "Salles modernes de formation dans 10 grandes métropoles africaines",
        },
        {
            id:"P3",
            imgsrc: iconInvestS02_3,
            title: "3 000 000",
            content: "Clients sur le continent avec notre plateforme e-learning panafricaine",
        },
        {
            id:"P4",
            imgsrc: iconInvestS02_4,
            title: "25",
            content: "Pays couverts par notre plateforme e-learning",
        },
        {
            id:"P5",
            imgsrc: iconInvestS02_5,
            title: ["4 935 638 313 FCFA",<br/>,"8 813 639 $US"],
            content: "De bénéfices bruts hors impots sur 3 ans",
        },
        {
            id:"P6",
            imgsrc: iconInvestS02_6,
            title: ["10 526 092 978 FCFA",<br/>,"18 796 594 $US"],
            content: "De chiffre d'affaires prévisionnel sur 03 ans",
        },
        
    ],
    adaaForInvestS5Grid : [
        {
            id:"P1",
            imgsrc: iconInvestS05_1,
            title: "Déjà 11 pays couverts",
            content: ["04 avec des formations présentielles ", <a href="https://www.adaa-academie.com">www.adaa-academie.com</a>," et 11 avec les formations en ligne ", <br/>,"(", <a href="https://www.adaalearning.com">www.adaalearning.com</a>, ")"],
        },
        {
            id:"P2",
            imgsrc: iconInvestS05_2,
            title: "Croissance annuelle moyenne d’au moins 120%",
            content: "19 000 000 FCFA de chiffres d’affaires en 2019, 49 000 000 FCFA en 2020 et 101 000 000 FCFA en 2021",
        },
        {
            id:"P3",
            imgsrc: iconInvestS05_3,
            title: ["Une valuation de plus de ",<br/>,"2 500 000 000 FCFA (4 464 285 $US) au 30.11.2021 :"],
            content: "Notre potentiel technologique et notre croissance continue permettent à notre entreprise chaque jour de gagner en valeur rapidement",
        },
        {
            id:"P4",
            imgsrc: iconInvestS05_4,
            title: "Déjà 15 Parcours d’apprentissage disponibles sur AdaaLearning",
            content: "Devenez Développeur web, Devenez Administrateur Système Linux, Devenez Monteur vidéo, etc",
        },
        {
            id:"P5",
            imgsrc: iconInvestS05_5,
            title: "+25 000 personnes déjà formées",
            content: "En présentiel, en ligne et à distance",
        },
        {
            id:"P6",
            imgsrc: iconInvestS05_6,
            title: "+15 grandes Entreprises déjà formées",
            content: "BGFI, Olam, GSEZ, MUPECI, SCR MAYA, CANAL DE VIE, ECOBANK,  AXA, FRIESLANDER, SABC, CNSS, etc",
        },
        
    ],
    adaaForInvestS6Grid : [
        {
            imgsrc: iconInvestS06_1,            
        },
        {
            imgsrc: iconInvestS06_2,            
        },
        {
            imgsrc: iconInvestS06_3,            
        },
        {
            imgsrc: iconInvestS06_4,            
        },
        {
            imgsrc: iconInvestS06_5,            
        },
        {
            imgsrc: iconInvestS06_6,            
        },
        {
            imgsrc: iconInvestS06_7,            
        },
        {
            imgsrc: iconInvestS06_8,            
        },
        {
            imgsrc: iconInvestS06_9,            
        },
        {
            imgsrc: iconInvestS06_10,            
        },
        {
            imgsrc: iconInvestS06_11,            
        },
        {
            imgsrc: iconInvestS06_12,            
        },
        {
            imgsrc: iconInvestS06_13,            
        },
        {
            imgsrc: iconInvestS06_14,            
        },
        {
            imgsrc: iconInvestS06_15,            
        },
        {
            imgsrc: iconInvestS06_16,            
        },
    ],
    adaaForInvestS11Grid : [
        {
            id:"is11_1",
            imgsrc: image1,
            title: "www.adaalearning.com",
            content: "La plus grande place de marché africaine pour les formations certifiantes, accesibles à partir de 5 $US",
        },
        {
            id:"is11_2",
            imgsrc: image2,
            title: "www.adaa-academie.com",
            content: "Notre volonté depuis 3 ans à offrir des formations présentielles de pointes de pointe aux entreprises, cadres, chefs d’entreprises et chercheurs d’emploi. Déjà 2 salles modernes de formation aménagés",
        },
        {
            id:"is11_3",
            imgsrc: image3,
            title: "www.adaajobs.com",
            content: "Plateforme d’accompagnement des entreprises en Afrique et Europe pour le recrutement des talents de qualité dans le digital, l’informatique et les technologies",
        },
    ],
    adaaForInvestS14Grid : [
        {
            id:"1",
            content: "Entreprise : ADAA SAS",
            img: iconInvestS14_1,
        },
        {
            id:"2",
            content: "Montant de la levée de fonds:  650 000 000 Fcfa (1 150 442 $US)",
            img: iconInvestS14_2,
        },
        {
            id:"3",
            content: "Part du capital ouvert : 35 %",
            img: iconInvestS14_3,
        },
        {
            id:"4",
            content: "Nombre de parts nominatives disponibles : 65 000",
            img: iconInvestS14_4,
        },
        {
            id:"5",
            content: "Valeur de l’action : 10 500 Fcfa",
            img: iconInvestS14_5,
        },
        {
            id:"6",
            content: "Nombre minimum d’actions à la souscription : 50",
            img: iconInvestS14_6,
        },
        {
            id:"7",
            content: "Période de la levée de fonds : 06 Décembre 2021 au 06 Mars 2022",
            img: iconInvestS14_7,
        },
        {
            id:"8",
            content: "Personnes éligibles : Chefs d’entreprises, Cadres et Professionnels, Business Angel, Fonds d’investissements",
            img: iconInvestS14_8,
        },
        {
            id:"9",
            content: "Tenue de l’Assemblée Générale d’augmentation de Capital avec tous les actionnaires : Samedi 12 Mars 2022",
            img: iconInvestS14_9,
        },
    ],
    adaaForInvestS16Grid : [
        {
            id:"1",
            content: ["Enregistrez votre intention d’investissement via le formulaire accessible ici ", <a href='#'>www.investir.adaalearning.com</a>, " et choisisez une date et heure de rendez-vous de d’échange avec le CEO ADAA SAS"],
            img:iconInvestS16_1,
        },
        {
            id:"2",
            content: ["Vous recevez sous 24h par mail, une lettre d’acceptation de votre demande d’investissement et  de confirmation de la disponibilité du nombre de parts sollicitées ", <strong>+ Mémo d’investissement + Business Plan simplifié</strong>],
            img:iconInvestS16_2,
        },
        {
            id:"3",
            content: "Entretien Zoom individuel avec Mathurin KOUGUEM, CEO ADAA SAS, pour répondre à toutes vos questions et vous présenter en détail notre business plan et notre plan de croissance.",
            img:iconInvestS16_3,
        },
        {
            id:"4",
            content: ["Vous faites un versement ou virement du montant global du nombre de parts sollicités au bénéfice de ADAA SAS sur l’un de ses 02 comptes bancaires qui vous seront indiqués (Côte d’Ivoire ou Cameroun suivant votre préférence)", <br/>, <span className="mini">N.B: Le nbre d’actions souscrites n’est confirmé qu’à la date de versement ou virement (N.B: La levée de fonds devant être clôturée dés atteinte de l’objectif).</span> ],
            img:iconInvestS16_4,
        },
        {
            id:"5",
            content: ["Vous nous envoyez une copie de votre reçu de versement ou ordre de virement à l’adresse ",<a href="#">investors@adaalearning.com</a>, " ainsi qu'une copie investisseur signé par vos soins."],
            img:iconInvestS16_5,
        },
        {
            id:"6",
            content: "Nous vous envoyons systématiquement un mail dés vérification confirmée du crédit du compte bancaire ADAA SAS du montant de votre versement ou virement + Bulletin de souscription rempli et signé par ADAA.",
            img:iconInvestS16_6,
        },
        {
            id:"7",
            content: "Le 12 Mars 2022, nous tiendrons une AGE d’augmentation de capital avec tous les actionnaires et 1 mois après (15 Avril 2022) nous vous enverrons par courrier postal votre TITRE NOTARIAL pour les actions souscrites dans ADAA SAS.",
            img:iconInvestS16_7,
        }
    ],
    adaaForInvestS17Grid : [
        {
            id: "1",
            name: "Mathurin KOUGUEM",
            role: "Co-Fondateur & CEO ADAA SAS",
            img: iconInvestS17_1,
        },
        {
            id: "2",
            name: "Adrien OYONO",
            role: ["Co-Fondateur & Chief Technical Officer (CTO)",<br/>,"Associé"],
            img: iconInvestS17_2,
        },
        {
            id: "3",
            name: "Doriméne WOPIWO",
            role: ["Corporate Business Manager",<br/>,"Associé"],
            img: iconInvestS17_3,
        },
        {
            id: "4",
            name: "Patrick MBENOUN",
            role: ["Directeur Administratif et Financier",<br/>,"Associé"],
            img: iconInvestS17_4,
        },
        {
            id: "5",
            name: "Aristide MANYAKA",
            role: ["Marketing & Sales Manager",<br/>,"Associé"],
            img: iconInvestS17_5,
        },
        {
            id: "6",
            name: "Serge SIMO",
            role: ["Project Manager",<br/>,"Associé"],
            img: iconInvestS17_6,
        },
        {
            id: "7",
            name: "Marie Thérese BITIND",
            role: "Executive Assistant / HR",
            img: iconInvestS17_7,
        },
        {
            id: "8",
            name: "Blanche LEUGA",
            role: ["Country Manager Cameroun",<br/>,"Associé"],
            img: iconInvestS17_8,
        },
        {
            id: "9",
            name: "Anzara KOUAME",
            role: ["Country Manager Côte d’Ivoire",<br/>, "Associé"],
            img: iconInvestS17_9,
        },
        
    ],    
};

export default Sdata;