import React from "react";

import "./style_oneGridItemInvest.css";
import one_img1 from "../../../../imgs/adaaForInvest/dirig2.png";

const OneGridItem = ({one_id, one_img, one_name, one_role}) => {
  return (
    <>
    <div class="item">
        <div class="grid-container-item-section17_invest">
            <div className="box-left-s17_invest">
                <div className="perso_s17_invest" style={{background: "url("+ one_img + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}></div>
            </div>
            <div className="box-right-s17_invest">
                <span>{one_name}</span>{one_role}
            </div>
        </div>
    </div>
    </>
  );
}

export default OneGridItem;