import React from "react";

import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import OneGridItem from "./OneGridItemS17Invest/oneGridItemInvest";

import Sdata from '../../../sdata/Sdata';

import "./style_sectionSeventeenInvest.css";


const SectionSeventeenInvest = () => {
  return (
    <>
        <section className="section17_invest">

            <div className="section17_invest_title">
                <span className="section17_invest_title_text_big satisfy_font">L’équipe dirigeante</span>
            </div>
            {/* <div className="section02_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}

            <div className="section17_invest_container">
            <div class="grid-container-section17_invest ">
                {
                    Sdata.adaaForInvestS17Grid.map((val, ind) => {
                        return <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.img}
                                one_name={val.name}
                                one_role={val.role}
                                />
                    })
                }                
                
            </div>

            </div>

            
        </section>
    </>
  );
}

export default SectionSeventeenInvest;