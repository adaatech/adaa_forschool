import {React} from "react";
import {Link} from "react-router-dom";

import "./style_sectionFourInvest.css";

import bg_picture_1 from "../../../imgs/adaaForInvest/Image1.jpg";
import bg_picture_2 from "../../../imgs/adaaForInvest/Image2.png";
import one_icon from "../../../imgs/adaaForInvest/icon1.png";


function SectionFour() {
  return (
    <section className="section04_invest">
        {/* <img className="bg_picture_s1_business " src={bg_picture} alt="background_picture"/> */}
        
        <div className="container_s4_invest">
            
            <img className="bg_picture_s4_invest " src={bg_picture_1} alt="background_picture"/>
            
            <div className="container_text_s4_invest" style={{background: "url("+ bg_picture_2 + ") center center no-repeat", backgroundSize: "contain", backgroundPosition: "center"}}>
                <div className="box-top-s4_invest">
                    <img className="one_imgs4_invest" src={one_icon} alt=""/>
                </div>
                <div className="box-bottom-s4_invest">
                    <div className="box-bottom-title-s4_invest">ADAA SAS pour sa gouvernance est dotée d’un CONSEIL D’ADMINISTRATION qui siège 4 fois/an et tient son Assemblée Générale 2 fois l’an.</div>                    
                </div>
            </div>
                               
        </div>
    </section>
  );
}

export default SectionFour;