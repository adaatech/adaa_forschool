import React from "react";

import OneGridItem from "./OneGridItemS2Invest/oneGridItemInvest";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionTwoInvest.css";


const SectionTwoInvest = () => {
  return (
    <>
        <section className="section02_invest">

            <div className="section02_invest_title">
                <span className="section02_invest_title_text_big satisfy_font">Notre ambition sur 3 ans</span>
                <span className="section02_invest_title_text_small">(Avril 2022 - Avril 2025)</span>
                <div className="section02_invest_title_underline"></div>
            </div>
            {/* <div className="section02_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}
            
            <div class="grid-container-section02_invest ">
                {

                    Sdata.adaaForInvestS2Grid.map((val, ind) => {
                        return <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_title={val.title}
                                one_content={val.content}
                                one_ind = {ind}
                                />
                    })
                }

            </div>
        </section>
    </>
  );
}

export default SectionTwoInvest;