import React from "react";

import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import OneStepLineGridItem from "./OneGridItemS16Invest/oneStepLineGridItemInvest";
import OneLeftGridItem from "./OneGridItemS16Invest/oneLeftGridItemInvest";
import OneRightGridItem from "./OneGridItemS16Invest/oneRightGridItemInvest";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionSixteenInvest.css";

import bg_picture_0 from "../../../imgs/adaaForInvest/001.png";

const SectionSixteenInvest = () => {
  return (
    <>
        <section className="section16_invest">
        <img className="bg_picture0_s16_invest " src={bg_picture_0} alt="background_picture"/>
            <div className="section16_invest_title">
                <span className="section16_invest_title_text_big satisfy_font">Je souhaite investir, que faire ?</span>
                <span className="section16_invest_title_text_small">Vous êtes décidé à rejoindre la belle aventure ADAA ? <br/> Les principales étapes sont les suivantes : </span>
                {/* <div className="section16_invest_title_underline"></div> */}
            </div>
            {/* <div className="section02_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}

            <div className="section16_invest_container">
            <div class="grid-container-section16_invest ">
                <div class="grid-container-stepLine-section16_invest ">
                    {
                        Sdata.adaaForInvestS16Grid.map((val, ind) => {
                            return <OneStepLineGridItem
                                    key={ind}
                                    one_index={ind}
                                    />
                        })
                    }
                </div>
                <div class="grid-container-left-section16_invest ">
                    {
                        Sdata.adaaForInvestS16Grid.map((val, ind) => {
                            return <OneLeftGridItem
                                    key={ind}
                                    one_img={val.img}
                                    />
                        })
                    }
                </div>
                <div class="grid-container-right-section16_invest ">
                    {
                        Sdata.adaaForInvestS16Grid.map((val, ind) => {
                            return <OneRightGridItem
                                    key={ind}
                                    one_content={val.content}
                                    />
                        })
                    }
            </div>
            </div>

            </div>

            
        </section>
    </>
  );
}

export default SectionSixteenInvest;