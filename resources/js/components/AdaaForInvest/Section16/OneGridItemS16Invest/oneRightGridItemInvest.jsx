import React from "react";

import "./style_oneGridItemInvest.css";
import one_img1 from "../../../../imgs/adaaForSchool/icon5.png";

const OneGridItem = ({one_id, one_img, one_title, one_content}) => {
  return (
    <>
    <div class="item">
        <div className="box-right-s16_invest">
            {one_content}
        </div>
    </div>
    </>
  );
}

export default OneGridItem;