import React from "react";

import "./style_oneGridItemInvest.css";
import one_img1 from "../../../../imgs/adaaForSchool/icon5.png";

const OneGridItem = ({one_id, one_img, one_title, one_content, one_index}) => {
  return (
    <>
    <div class="item">
        <div className="box-stepLine-s16_invest">
          <span>{one_index}</span>
        </div>        
    </div>
    </>
  );
}

export default OneGridItem;