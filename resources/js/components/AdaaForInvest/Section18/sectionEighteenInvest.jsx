import React from "react";

import TextInput from "../../Common/InputOnly/TextInput/textInput";
import PhoneInput from "../../Common/InputOnly/PhoneInput/phoneInput";
import EmailInput from "../../Common/InputOnly/EmailInput/emailInput";
import TextArea from "../../Common/InputOnly/TextArea/textArea";
import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";

import Sdata from '../../../sdata/Sdata';

import one_img1 from "../../../imgs/adaaForSchool/icon5.png";
import "./style_sectionEighteenInvest.css";


const SectionEighteenInvest = () => {
  return (
    <>
        <section className="section18_invest">

            <div className="section18_invest_title">
                <span className="section18_invest_title_text_big satisfy_font">Nous contacter</span>
                <span className="section18_invest_title_text_small">Besoin de plus de détails ? Vous avez des questions ? N’hésitez pas à nous contacter.<br/> Nous nous ferons le plaisir de vous répondre dans les plus brefs délais :</span>
            </div>
            {/* <div className="section02_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}

            <div className="section18_invest_container">
                <div class="grid-container-section18_invest ">
                    <div class="itemLeft">
                        <div class="grid-container-itemLeft-section18_invest">

                            <div className="box-left-s18_invest">
                                <img className="one_img" src={one_img1} alt=""/>
                                {/* <div className="perso_s18_invest" style={{background: "url("+ one_img1 + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}></div> */}
                            </div>
                            <div className="box-right-s18_invest">
                                {/* {one_title} */}
                                <span>www.adaalearning.com<br/>www.adaa-academie.com<br/>www.adaajobs.com</span>
                            </div>
                        </div>

                        <div class="grid-container-itemLeft-section18_invest">
                            <div className="box-left-s18_invest">
                                <img className="one_img" src={one_img1} alt=""/>
                                {/* <div className="perso_s18_invest" style={{background: "url("+ one_img1 + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}></div> */}
                            </div>
                            <div className="box-right-s18_invest">
                                {/* {one_title} */}
                                ADAA CAMEROUN <br/>
                                2éme et 3étage Immeuble Pharmacie de Logpom – Logpom, Douala <br/> 
                                Téléphone : +237 699 06 68 31<br/>
                                Email: investors@adaalearning.com
                            </div>
                        </div>

                        <div class="grid-container-itemLeft-section18_invest">
                            <div className="box-left-s18_invest">
                                <img className="one_img" src={one_img1} alt=""/>
                                {/* <div className="perso_s18_invest" style={{background: "url("+ one_img1 + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}></div> */}
                            </div>
                            <div className="box-right-s18_invest">
                                {/* {one_title} */}
                                ADAA COTE D’IVOIRE <br/>
                                Polyclinique des 2 Plateaux – Abidjan, Côte d’Ivoire <br/> 
                                Téléphone : +225 0708568709 <br/>
                                Email: investors@adaalearning.com
                            </div>
                        </div>
                    </div>
                    <div className="itemRight">
                    <form action="">
                        <div class="grid-container-itemRight-section18_invest ">
                            <TextInput  nameText={"nom"} grayColor={"True"} placeholderText={"Votre nom"} requiredValue={"True"}/>
                            <PhoneInput nameText={"telephone"} grayColor={"True"} placeholderText={"Téléphone"} requiredValue={"True"}/>
                            <EmailInput nameText={"email"} grayColor={"True"} placeholderText={"Email"} requiredValue={"True"}/>
                            <TextInput  nameText={"requete"} grayColor={"True"} placeholderText={"Objet de votre requête"} requiredValue={"True"}/>
                            <TextArea  rowsValue={"5"} grayColor={"True"} nameText={"nom"} placeholderText={"Votre message"} requiredValue={"True"}/>
                        </div>
                        <div className="submit_btn_container_s18_invest">
                            <PrimaryBtn text={"Envoyer"} submit={"True"}/>
                        </div>
                    </form>
                    </div>
                </div>

            </div>

            
        </section>
    </>
  );
}

export default SectionEighteenInvest;