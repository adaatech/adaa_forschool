import React from "react";

import "./style_oneGridItemInvest.css";
import one_img2 from "../../../../imgs/adaaForSchool/icon5.png";

const OneGridItem = ({one_id, one_img, one_title, one_content}) => {
  return (
    <>
    <div class="item">
        <div className="box-bg-s2_invest"></div>
        <div className="box-top-s2_invest"> 
            <img className="one_img" src={one_img2} alt=""/>
        </div>
        <div className="box-bottom-s2_invest">
            <div className="box-bottom-title-s2_invest">{one_title}</div>
            <div className="box-bottom-content-s2_invest">{one_content}</div>
        </div>
    </div>
    </>
  );
}

export default OneGridItem;