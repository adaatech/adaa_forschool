import React from "react";

import Slider from "../../Common/Sliders/SliderRaison/SliderRaison";
import OneGridItem from "./OneGridItemS15Invest/oneGridItemInvest";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionFifteenInvest.css";


const SectionFifteenInvest = () => {
  return (
    <>
        <section className="section15_invest">

            <div className="section15_invest_title">
                <span className="section15_invest_title_text_big satisfy_font">12 Raisons d’investir dans Adaa Sas</span>               
            </div>

            <Slider/> 
        </section>
    </>
  );
}

export default SectionFifteenInvest;