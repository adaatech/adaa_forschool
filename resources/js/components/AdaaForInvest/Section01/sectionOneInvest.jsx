import {React} from "react";
import {Link} from "react-router-dom";

import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import "./style_sectionOneInvest.css";

import bg_picture_1 from "../../../imgs/adaaForInvest/Image7.png";
import bg_picture_2 from "../../../imgs/adaaForBusiness/Image2.png";
import one_img from "../../../imgs/adaaForInvest/icon4.png";
import one_img_2 from "../../../imgs/adaaForInvest/circle_gold.png";
import one_img_3 from "../../../imgs/adaaForInvest/ndop-bar.jpg";

import video from '../../../videos/video_S1_invest.mp4';

// style={{background: "url("+ bg_picture_1 + ") center center no-repeat", backgroundSize: "100% auto", backgroundPosition: "center"}}

function SectionOneInvest() {
  return (
    <section className="section01_invest">
        
        <div className="container_s1_invest">
            <div className="boxLeft_s1_invest">
                <img className="bg_picture_s1_invest " src={bg_picture_1} alt="background_picture"/>
                <img className="bg_picture_left_s1_invest " src={one_img_3} alt="background_picture"/>
                <img className="bg_picture_right_s1_invest " src={one_img_3} alt="background_picture"/>
                
                <div className="boxLeft_text_s1_invest" style={{background: "url("+ one_img_2 + ") center center no-repeat", backgroundSize: "100% auto", backgroundPosition: "center"}}>
                    <div className="boxLeft_text_s1_invest_container">
                        <div className="box-top-s1_invest">
                            <img className="one_imgs1_invest" src={one_img} alt=""/>
                        </div>
                        <div className="box-bottom-s1_invest">
                            <div className="box-bottom-title-s1_invest">Démocratisons ensemble la compétence en Afrique</div>
                            <div className="box-bottom-content-s1_invest">et changeons des millions de vies 
    sur l’ensemble du continent africain</div>
                        </div>
                    </div>
                    
                </div>

                <div className="boxLeft_s1_investBtn_container">
                    <TernaryBtn text={"Devenez investisseur"} link={"#"}/>
                </div>
            </div>
            <div className="boxRight_s1_invest">
            {/* <img className="bg_picture_s1_invest " src={bg_picture_2} alt="background_picture"/>          */}
            <video width="auto" height="100%" controls >
                <source src={video} type="video/mp4"/>
            </video>
            </div>           
        </div>
    </section>
  );
}

export default SectionOneInvest;