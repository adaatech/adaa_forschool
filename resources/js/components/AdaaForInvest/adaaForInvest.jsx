import React from "react";

import SectionOne from "./Section01/sectionOneInvest";
import SectionTwo from "./Section02/sectionTwoInvest";
import SectionThree from "./Section03/sectionThreeInvest";
import SectionFour from "./Section04/sectionFourInvest";
import SectionFive from "./Section05/sectionFiveInvest";
import SectionSix from "./Section06/sectionSixInvest";
import SectionSeven from "./Section07/sectionSevenInvest";
import SectionEight from "./Section08/sectionEightInvest";
import SectionNine from "./Section09/sectionNineInvest";
import SectionTen from "./Section10/sectionTenInvest";
import SectionEleven from "./Section11/sectionElevenInvest";
import SectionTwelve from "./Section12/sectionTwelveInvest";
import SectionThirteen from "./Section13/sectionThirteenInvest";
import SectionFourteen from "./Section14/sectionFourteenInvest";
import SectionFifteen from "./Section15/sectionFifteenInvest";
import SectionSixteen from "./Section16/sectionSixteenInvest";
import SectionSeventeen from "./Section17/sectionSeventeenInvest";
import SectionEighteen from "./Section18/sectionEighteenInvest";
import SectionNineteen from "./Section19/sectionNineteenInvest";

function AdaaForInvest() {
  return (
      <>
    <SectionOne/>
    <SectionTwo/>
    <SectionThree/>
    <SectionFour/>
    <SectionFive/>
    <SectionSix/>
    <SectionSeven/>
    <SectionEight/>
    <SectionNine/>
    <SectionTen/>
    <SectionEleven/>
    <SectionTwelve/>
    <SectionThirteen/>
    <SectionFourteen/>
    <SectionFifteen/>
    <SectionSixteen/>
    <SectionSeventeen/>
    <SectionEighteen/>
    <SectionNineteen/>
    </>
  );
}

export default AdaaForInvest;
