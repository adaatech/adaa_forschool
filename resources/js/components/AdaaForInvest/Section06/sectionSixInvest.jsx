import React from "react";

import Sdata from '../../../sdata/Sdata';

import oneIcon from "../../../imgs/adaaForInvest/partners/olam.png";
import bg_picture_0 from "../../../imgs/adaaForInvest/004.png";
import bg_picture_2 from "../../../imgs/adaaForInvest/008.png";
import one_img from "../../../imgs/adaaForSchool/icon1.png";

import "./style_sectionSixInvest.css";



const SectionSixInvest = () => {
  return (
    <>
        <section className="section06_invest">
        <img className="bg_picture0_s6_invest " src={bg_picture_0} alt="background_picture"/>
            <div className="section06_invest_title">
                <span className="section06_invest_title_text_big satisfy_font">Nous avons construit et déployé la place de marché africaine N°1 pour l'apprentissage et la formation</span>
                {/* <span className="section06_invest_title_text_small">(2019 à nos jours)</span>
                <div className="section06_invest_title_underline"></div> */}
            </div>
            {/* <div className="section02_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}

            <div className="container_s6_invest">
                <div className="boxLeft_s6_invest">
                    {/* <img className="bg_picture_s6_invest " src={bg_picture_1} alt="background_picture"/> */}
                    <div className="boxLeft_text_s6_invest">
                    Notre engagement premier est de rendre les connaissances et l’expertise accessibles à la majorité sur le Continent. Nous donnons la possibilité à des formateurs de plusieurs pays dans le monde de pouvoir partager leurs connaissances, expériences et expertises avec des apprenants désireux d’en acquérir.  Nous aidons des personnes à transformer leurs vies par l’apprentissage.
                    </div>
                </div>
                <div className="boxRight_s6_invest">
                    <img className="bg_picture_s6_invest " src={bg_picture_2} alt="background_picture"/>         
                
                    <div className="boxRight_text_s6_invest">
                        <div className="box-bottom-s6_invest">
                            <div className="box-bottom-title-s6_invest">Avec <span>+27</span> <br/> moyens de paiement mobile disponibles pour 12 pays, en plus du paiement par carte.</div>                            
                        </div>
                    </div>
                </div>           
            </div>

            <div className="containerBottom_s6_invest">
            {

                Sdata.adaaForInvestS6Grid.map((val, ind) => {
                    return (
                        <div className="containerBottom_icon_s6_invest" style={{background: "url("+ val.imgsrc + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}>
                        </div>
                    )
                })
                }
            </div>
            
            
        </section>
    </>
  );
}

export default SectionSixInvest;