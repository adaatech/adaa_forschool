import React from "react";


import bg_picture_1 from "../../../imgs/adaaForInvest/Image5.jpg";
import one_img_1 from "../../../imgs/adaaForInvest/build-africa.png";
import "./style_sectionNineteenInvest.css";

import bg_picture_0 from "../../../imgs/adaaForInvest/002.png";

const SectionNineteenInvest = () => {
  return (
    <>
        <section className="section19_invest">
            
            <div className="section19_invest-container" style={{background: "url("+ bg_picture_1 + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}>
                {/* <img className="bg_picture_s19_invest " src={bg_picture_1} alt="background_picture"/> */}
                <div className="container_text_s19_invest" style={{background: "url("+ one_img_1 + ") center center no-repeat", backgroundSize: "contain", backgroundPosition: "center"}}>
                        {/* <div className="box-top-s19_invest">
                            <img className="one_imgs19_invest" src={one_img} alt=""/>
                        </div>
                        <div className="box-bottom-s19_invest">
                            <div className="box-bottom-title-s19_invest">LET’S BUILD <br/> AFRICA <br/> TOGETHER</div>                        
                        </div> */}
                    </div>
                </div>
                <img className="bg_picture0_s19_invest " src={bg_picture_0} alt="background_picture"/>
        </section>
    </>
  );
}

export default SectionNineteenInvest;