import React from "react";

import SliderHistoire from "../../Common/Sliders/SliderHistoire/SliderHistoire";
// import OneGridItem from "./OneGridItemS3Invest/oneGridItemInvest";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionThreeInvest.css";


const SectionThreeInvest = () => {
  return (
    <>
        <section className="section03_invest">

            <div className="section03_invest_title">
                <span className="section03_invest_title_text_big satisfy_font">Notre histoire, découvrez là</span>
                <span className="section03_invest_title_text_small">(2019 à nos jours)</span>
                <div className="section03_invest_title_underline"></div>
            </div>
            <div className="sliderContainer">
            <SliderHistoire/>
            </div>
            
        </section>
    </>
  );
}

export default SectionThreeInvest;