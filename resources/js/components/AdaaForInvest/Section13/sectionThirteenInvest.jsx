import React from "react";

import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import SliderEquipe from "../../Common/Sliders/SliderEquipe/SliderEquipe";

import "./style_sectionThirteenInvest.css";


const SectionThirteenInvest = () => {
  return (
    <>
        <section className="section13_invest">

            <div className="section13_invest_title">
                <span className="section13_invest_title_text_big satisfy_font">Notre équipe</span>
                <span className="section13_invest_title_text_small">Aujourd’hui composée de 15 personnes, nous envisageons dés Mars 2022 agrandir les effectifs avec la création de la Holding sur Abidjan en Côte d’Ivoire</span>
                
            </div>
            <SliderEquipe/>

            <div className="container_s13_investBtn_container">
                <TernaryBtn text={"Devenez investisseur"} link={"#"}/>
            </div>
        </section>
    </>
  );
}

export default SectionThirteenInvest;