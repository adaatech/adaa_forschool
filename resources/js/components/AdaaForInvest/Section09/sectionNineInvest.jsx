import {React} from "react";
import {Link} from "react-router-dom";

import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import "./style_sectionNineInvest.css";

import bg_picture_1 from "../../../imgs/adaaForInvest/Image7.png";
import bg_picture_2 from "../../../imgs/adaaForBusiness/Image2.png";
import one_img from "../../../imgs/adaaForSchool/icon1.png";
import video from '../../../videos/video_S9_invest.mp4'

function SectionNineInvest() {
  return (
    <section className="section09_invest">
        {/* <img className="bg_picture_s1_business " src={bg_picture} alt="background_picture"/> */}
        
        <div className="container_s9_invest">
            <div className="boxLeft_s9_invest">                
                <video width="100%" height="auto" controls >
                    <source src={video} type="video/mp4"/>
                </video>
            </div> 
            <div className="boxRight_s9_invest">            
                <div className="section09_invest_title">
                    <span className="section09_invest_title_text_big satisfy_font">Poursuivre notre expansion africaine</span>
                    {/* <span className="section09_invest_title_text_small">Poursuivre notre expansion africaine </span> */}
                </div>                   
                <div className="boxRight_text_s9_invest">
                avec une option de formations présentielles pour entreprises, cadres et chercheurs d’emplois dans les domaines dans un cadre confortable
                </div>
            
                <div className="boxRight_s9_investBtn_container">
                    <TernaryBtn text={"Devenez investisseur"} link={"#"}/>
                </div>
            </div>
                      
        </div>
    </section>
  );
}

export default SectionNineInvest;