import React from "react";

import "./style_oneGridItemInvest.css";
import one_img2 from "../../../../imgs/adaaForSchool/icon5.png";

const OneGridItem = ({one_id, one_img, one_title, one_content}) => {
  return (
    <>
    <div class="item">
        <div className="box-bg-s11_invest"></div>
        
        <div className="box-top-s11_invest">
            <div className="box-top-title-s11_invest">{one_title}</div>
            <div className="box-top-content-s11_invest">{one_content}</div>
        </div>
        <div className="box-bottom-s11_invest"> 
            <img className="one_img" src={one_img} alt=""/>
        </div>
    </div>
    </>
  );
}

export default OneGridItem;