import React from "react";

import OneGridItem from "./OneGridItemS11Invest/oneGridItemInvest";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionElevenInvest.css";


const SectionElevenInvest = () => {
  return (
    <>
        <section className="section11_invest">

            
            <div class="grid-container-section11_invest ">
                {

                    Sdata.adaaForInvestS11Grid.map((val, ind) => {
                        return <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_title={val.title}
                                one_content={val.content}
                                />
                    })
                }

            </div>
        </section>
    </>
  );
}

export default SectionElevenInvest;