import React from "react";

import Sdata from '../../../sdata/Sdata';

import oneIcon from "../../../imgs/adaaForInvest/partners/olam.png";
import bg_picture_2 from "../../../imgs/adaaForInvest/007.png";
import one_img from "../../../imgs/adaaForSchool/icon1.png";

import "./style_sectionSevenInvest.css";

import SlideShow from "../../Common/Sliders/SlideShow/SlideShow";

const SectionSevenInvest = () => {
  return (
    <>
        <section className="section07_invest">

            <div className="section07_invest_title">
                <span className="section07_invest_title_text_big satisfy_font">Nos loves Investors sont fiers du chemin parcouru avec ADAA SAS</span>                
            </div>
            <SlideShow/>            
            
        </section>
    </>
  );
}

export default SectionSevenInvest;