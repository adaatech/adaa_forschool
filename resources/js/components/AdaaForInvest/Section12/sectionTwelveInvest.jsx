import {React} from "react";
import {Link} from "react-router-dom";

import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import "./style_sectionTwelveInvest.css";

import bg_picture from "../../../imgs/adaaForInvest/Image4.png";

function SectionTwelveInvest() {
  return (
    <section className="section12_invest">
        
        <div className="container_s12_invest">
            <div className="boxLeft_s12_invest">                
                <img className="bg_picture_s12_invest " src={bg_picture} alt="background_picture"/>
            </div> 
            <div className="boxRight_s12_invest">            
                <div className="section12_invest_title">
                    <span className="section12_invest_title_text_big satisfy_font">Continuez d’apprendre,<br/> même sans connexion internet</span>
                    {/* <span className="section12_invest_title_text_small">Poursuivre notre expansion africaine </span> */}
                </div>                   
                <div className="boxRight_text_s12_invest">
                Nous offrons la possibilité aux apprenants de télécharger et de suivre plus tard leurs formations sans connexion internet via l’Application mobile Adaa Learning.
                </div>
            </div>
                      
        </div>
    </section>
  );
}

export default SectionTwelveInvest;