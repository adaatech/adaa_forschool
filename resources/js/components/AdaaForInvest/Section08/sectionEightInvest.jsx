import React from "react";

import Sdata from '../../../sdata/Sdata';

import oneIcon from "../../../imgs/adaaForInvest/partners/olam.png";
import bg_picture_2 from "../../../imgs/adaaForInvest/006.png";
import one_img from "../../../imgs/adaaForSchool/icon1.png";

import "./style_sectionEightInvest.css";


const SectionEightInvest = () => {
  return (
    <>
        <section className="section08_invest">            
            <div className="container_s8_invest">
                <div className="boxLeft_s8_invest">
                    <div className="section08_invest_title">
                        <span className="section08_invest_title_text_big satisfy_font satisfy_line_height">+3500</span>
                        <span className="section08_invest_title_text_small satisfy_font satisfy_line_heightt">Experts Formateurs issus de +35 pays dans le monde</span>
                    </div>                   
                    <div className="boxLeft_text_s8_invest">
                    La plus grande communauté d’Experts Formateurs en Afrique, au service du développement des Hommes et de l’Humanité
                    </div>
                </div>
                <div className="boxRight_s8_invest">
                    <img className="bg_picture_s8_invest " src={bg_picture_2} alt="background_picture"/>                
                </div>           
            </div>            
            
        </section>
    </>
  );
}

export default SectionEightInvest;