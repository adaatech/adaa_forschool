import React from "react";

import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import OneLeftGridItem from "./OneGridItemS14Invest/oneLeftGridItemInvest";
import OneRightGridItem from "./OneGridItemS14Invest/oneRightGridItemInvest";
import Sdata from '../../../sdata/Sdata';

import bg_picture_0 from "../../../imgs/adaaForInvest/003.png";

import "./style_sectionFourteenInvest.css";


const SectionFourteenInvest = () => {
  return (
    <>
        <section className="section14_invest">
        <img className="bg_picture0_s14_invest " src={bg_picture_0} alt="background_picture"/>
            <div className="section14_invest_title">
                <span className="section14_invest_title_text_big satisfy_font">La levée de fonds de Adaa Sas</span>
                <span className="section14_invest_title_text_small">Informations pratiques sur la levée de fonds de ADAA :</span>                
            </div>
            

            <div className="section14_invest_container">
            <div class="grid-container-section14_invest ">
                <div class="grid-container-left-section14_invest ">
                    {
                        Sdata.adaaForInvestS14Grid.map((val, ind) => {
                            return <OneLeftGridItem
                                    key={ind}
                                    one_img={val.img}
                                    />
                        })
                    }
                </div>
                <div class="grid-container-right-section14_invest ">
                    {
                        Sdata.adaaForInvestS14Grid.map((val, ind) => {
                            return <OneRightGridItem
                                    key={ind}
                                    one_content={val.content}
                                    />
                        })
                    }
            </div>
            </div>

            </div>

            
        </section>
    </>
  );
}

export default SectionFourteenInvest;