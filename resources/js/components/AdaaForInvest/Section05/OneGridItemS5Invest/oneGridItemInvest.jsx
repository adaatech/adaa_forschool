import React from "react";

import "./style_oneGridItemInvest.css";
import one_img2 from "../../../../imgs/adaaForSchool/icon5.png";

const OneGridItem = ({key, one_id, one_img, one_title, one_content, oddItem}) => {
  return (
    <>
    <div className={`oneItem  ${oddItem? "oddItem":""}`}>
        <div className="box-bg-s5_invest"></div>
        <div className="box-top-s5_invest"> 
            <img className="one_img" src={one_img} alt=""/>
        </div>
        <div className="box-bottom-s5_invest">
            <div className="box-bottom-title-s5_invest">{one_title}</div>
            <div className="box-bottom-content-s5_invest">{one_content}</div>
        </div>
    </div>
    </>
  );
}

export default OneGridItem;