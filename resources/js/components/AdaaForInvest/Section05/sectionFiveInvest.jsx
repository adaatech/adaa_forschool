import React from "react";

import OneGridItem from "./OneGridItemS5Invest/oneGridItemInvest";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionFiveInvest.css";


const SectionFiveInvest = () => {
  return (
    <>
        <section className="section05_invest">

            <div className="section05_invest_title">
                <span className="section05_invest_title_text_big">QUELQUES CHIFFRES ET REALISATIONS CLES DE NOTRE ACTIVITE</span>
                
            </div>
            
            <div class="grid-container-section05_invest ">
                {

                    Sdata.adaaForInvestS5Grid.map((val, ind) => {
                        return (
                        ind%2 == 0 ?
                        <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_title={val.title}
                                one_content={val.content}
                                />
                        :
                        <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_title={val.title}
                                one_content={val.content}
                                oddItem={"True"}
                                />
                        )
                    })
                }

            </div>
        </section>
    </>
  );
}

export default SectionFiveInvest;