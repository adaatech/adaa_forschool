import React from "react";

import SliderTraining from "../../Common/Sliders/SliderTraining/SliderTraining";

import Sdata from '../../../sdata/Sdata';

import "./style_sectionTenInvest.css";


  

const SectionTenInvest = () => {
    
  return (
    <>
        <section className="section10_invest">

            <div className="section10_invest_title">
                <span className="section10_invest_title_text_big satisfy_font">Dans 4 pays, nous avons formé</span>
                <span className="section10_invest_title_text_small">Abidjan, Dakar, Libreville, Douala, Yaoundé sont les 5 grandes métropoles africaines où nous avons eu l’opportunité d’organiser des formations présentielles</span>                
            </div>
            <SliderTraining/>
        </section>
    </>
  );
}

export default SectionTenInvest;