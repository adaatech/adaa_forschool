import {React} from "react";

import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";
import "./style_sectionOne.css";

import bg_picture from "../../../imgs/adaaForSchool/Image1.png";

function SectionOneBusiness() {
  return (
    <section className="section01">
        <img className="bg_picture_s1 " src={bg_picture} alt="background_picture"/>
        <div className="teaser_container"> 
        <div className="teaser">              
                <div>
                    <p>Accélérer la modernisation des processus de formations au sein et hors de vos campus avec des solutions innovantes.</p>                    
                </div>
                <div className="s1_school_Btn_container">
                  <PrimaryBtn text={"Sollicitez une démo"} link={"#"}/>
                </div>                
        </div>
        </div>
    </section>
  );
}

export default SectionOneBusiness;