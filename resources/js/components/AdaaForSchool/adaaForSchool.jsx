import React from "react";
import ReactDOM from 'react-dom';

import SectionOne from "./Section01/sectionOne";
import SectionTwo from "./Section02/sectionTwo";
import SectionThree from "../Common/Categories/categories";
import SectionFour from "./Section04/sectionFour";
import SectionFive from "../Common/Form/formulaire";

function AdaaForSchool() {
  return (
    <>
      <SectionOne />
      <SectionTwo />
      <SectionThree />
      <SectionFour />
      <SectionFive />
    </>
  );
}

export default AdaaForSchool;
if (document.getElementById('root')) {
  ReactDOM.render(<AdaaForSchool />, document.getElementById('root'));
}
