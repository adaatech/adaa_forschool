import {React} from "react";

import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";
import "./style_sectionFour.css";

import bg_picture from "../../../imgs/adaaForSchool/Image12.png";

function SectionFour() {
  return (
    <section className="section04">
        <img className="bg_picture_s4 " src={bg_picture} alt="background_picture"/>
        <div className="teaser_s4">
            <div className="teaser_s4_container">               
                <div className="teaser_s4_title">
                    <span className="teaser_s4_title_text">Construisez votre campus en ligne aujourd'hui</span>                
                </div>
                <div className="teaser_s4_content">                
                    <p>Avec la solution Adaa For Schools et un service client dédié, votre Université/Ecole peut se développer et évoluer rapidement et facilement.</p>                    
                    <p>Choisissez des abonnements suivant vos besoins :
                    <ul>
                        <li>Option plateforme dédiée + accès illimité aux cours fournis par Adaa</li>
                        <li>Option  plateforme dédiée + accès illimité aux cours fournis par Adaa + production en toute autonomie des cours par l’Université (ou avec l’appui Adaa)</li>
                        <li>Option  plateforme dédiée + production en toute autonomie des cours par l’Université/Ecole (ou avec l’appui Adaa)</li>
                        <li>Et +10 autres options</li>
                    </ul>
                    </p>                
                </div>
                <div className="teaser_s4Btn_container">
                    <PrimaryBtn text={"Sollicitez une démo"} link={"#"}/>
                </div>
            </div>
        </div>
    </section>
  );
}

export default SectionFour;