import React from "react";

import OneGridItem from "./OneGridItemS2/oneGridItem";

import "./style_sectionTwo.css";
import bg_picture from "../../../imgs/adaaForSchool/Image11.jpg";

/* Icons */
import icon1 from '../../../imgs/adaaForSchool/icon1.png';
import icon2 from '../../../imgs/adaaForSchool/icon2.png';
import icon3 from '../../../imgs/adaaForSchool/icon3.png';
import icon4 from '../../../imgs/adaaForSchool/icon4.png';
import icon5 from '../../../imgs/adaaForSchool/icon5.png';
import icon6 from '../../../imgs/adaaForSchool/icon6.png';

const SectionTwo = () => {

    const adaaForSchoolS2Grid = [
        {
            id: "P1",
            imgsrc: icon1,
            title: "POSSEDEZ VOTRE PLATEFORME E-LEARNING",
            content: "Doter en 30 jours, votre Université/Ecole de sa propre plateforme  e-learning",
        },
        {
            id: "P2",
            imgsrc: icon2,
            title: "FAITES DES ECONOMIES",
            content: "Zéro tracasserie sur le développement d’une infrastructure e-learning",
        },
        {
            id: "P3",
            imgsrc: icon3,
            title: "AUGMENTEZ VOS EFFECTIFS ETUDIANTS",
            content: "Développez vos capacités d’accueil étudiants sans nouveaux investissements infrastructurels",
        },
        {
            id: "P4",
            imgsrc: icon4,
            title: "EXPLOITEZ ET CREEZ",
            content: "Exploiter les contenus déjà existants et créez vous-mêmes de nouveaux programmes attrayants pour vos étudiants",
        },
        {
            id: "P5",
            imgsrc: icon5,
            title: "OFFREZ DES DIPLOMES EN OPTION E-LEARNING",
            content: "Offrez des formations diplômantes et certifiantes en mode e-learning aux étudiants dans le monde",
        },
        {
            id: "P6",
            imgsrc: icon6,
            title: "OFFREZ PLUS DE POSSIBILITES",
            content: "A vos étudiants plus d'opportunités d'apprentissage et de chances d'explorer d’autres compétences",
        },

    ];
    return (
        <>
            <section className="section02">
                <img className="bg_picture_s2 " src={bg_picture} alt="background_picture" />
                <div className="bg_color_layer_s2"></div>

                <div className="section02_title">
                    <span className="section02_title_text_big">Adaa est le partenaire de votre université/école</span>
                    <span className="section02_title_text_small">&nbsp;pour vous apporter &nbsp;<u> l’infrastructure technologique e-learning </u> &nbsp; ainsi qu’un &nbsp; <u>catalogue large et riche de formations</u></span>
                </div>
                {/* <div className="section02_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}

                <div class="grid-container-section02 ">
                    {

                        adaaForSchoolS2Grid.map((val, ind) => {
                            return <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_title={val.title}
                                one_content={val.content}
                            />
                        })
                    }

                </div>
            </section>
        </>
    );
}

export default SectionTwo;