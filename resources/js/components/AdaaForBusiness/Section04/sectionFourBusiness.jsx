import {React} from "react";
import {Link} from "react-router-dom";

import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";
import "./style_sectionFourBusiness.css";

import bg_picture from "../../../imgs/adaaForBusiness/Image2.png";
import icon1 from "../../../imgs/adaaForBusiness/icon1.png";
import icon2 from "../../../imgs/adaaForBusiness/icon2.png";
import icon3 from "../../../imgs/adaaForBusiness/icon3.png";
import icon4 from "../../../imgs/adaaForBusiness/icon4.png";

function SectionFourBusiness() {
  return (
    <section className="section04_business" >
        <img className="bg_picture_s4_business " src={bg_picture} alt="background_picture"/>
        <div className="teaser_s4_business">
            <div className="teaser_s4_business_container">               
                <div className="teaser_s4_business_title">
                    <span className="teaser_s4_business_title_text">+DE COMPETENCES POUR VOS EQUIPES
                        <br/>+DE PRODUCTIVITE POUR VOTRE ENTREPRISE</span>             
                </div>
                <div className="teaser_s4_business_content">                
                    <p>Rendons ceci possible en quelques étapes simples et pratiques :</p>                                    
                </div>
                <div class="grid-container-section04-business ">
                    <div class="item_s4_business">
                        <div className="box-left-s4-business">
                            <img className="one_img" src={icon1} alt=""/>
                        </div>
                        <div className="box-right-s4-business">
                            <span className="box-right-title-s4-business">DISCUSSION ENTRE NOS 02 EQUIPES</span>
                            <span className="box-right-content-s4-business">Nos Experts Managers de formations entre en contact avec vos équipes en charge de la formation afin de discuter de vos besoins de formation, vos budgets, le profils des membres de votre équipe, vos objectifs de développement des compétences</span>
                        </div>
                    </div>
                    <div class="item_s4_business">
                        <div className="box-left-s4-business">
                            <img className="one_img" src={icon2} alt=""/>
                        </div>
                        <div className="box-right-s4-business">
                            <span className="box-right-title-s4-business">ANALYSE DE VOS BESOINS ET ELABORATION DE FORMULES DE FORMATIONS</span>
                            <span className="box-right-content-s4-business">Nos Experts Managers de formations, analysent vos besoins de formations et vous accompagnent dans le montage de FORMULES adaptées à vos équipes</span>
                        </div>
                    </div>
                    <div class="item_s4_business">
                        <div className="box-left-s4-business">
                            <img className="one_img" src={icon3} alt=""/>
                        </div>
                        <div className="box-right-s4-business">
                            <span className="box-right-title-s4-business">SIGNATURE DU CONTRAT  DE FORMATIONS</span>
                            <span className="box-right-content-s4-business">Une fois que vous avez analysé et validé les packs de formations construits avec l’appui de nos Managers de formations, nous signons le contrat de formations.</span>
                        </div>
                    </div>
                    <div class="item_s4_business">
                        <div className="box-left-s4-business">
                            <img className="one_img" src={icon4} alt=""/>
                        </div>
                        <div className="box-right-s4-business">
                            <span className="box-right-title-s4-business">COACHING ET ACCOMPAGNEMENT A LA PRISE EN MAIN</span>
                            <span className="box-right-content-s4-business">Nous vous dédions de façon permanente un Manager de formation qui vous formera vous et vos équipes à l’utilisation optimisée de la solution sur ordinateur et téléphone.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  );
}

export default SectionFourBusiness;