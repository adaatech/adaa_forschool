import React from "react";

import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import SliderFormateur from "../../Common/Sliders/SliderFormateur/SliderFormateur";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionFiveBusiness.css";


const SectionFiveBusiness = () => {
  return (
    <>
        <section className="section05_business">

            <div className="section05_business_title">
                <span className="section05_business_title_text_big">DES FORMATEURS PROFESSIONNELS DE CLASSE MONDIALE</span>
                <span className="section05_business_title_text_small">Faites bénéficier à vos équipes de l’expérience et de l’expertise de 1000+ formateurs, tous des experts dans leurs domaines respectifs</span>
                
            </div>
            <SliderFormateur/>

            {/* <div className="section02_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}
            
            {/* <div class="grid-container-section05_business ">
                {

                    Sdata.adaaForSchoolS2Grid.map((val, ind) => {
                        return <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_title={val.title}
                                one_content={val.content}
                                />
                    })
                }

            </div> */}
        </section>
    </>
  );
}

export default SectionFiveBusiness;