import React from "react";

import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";
// import Partners from "../../Common/Partners/Clients";
import SliderPartners from "../../Common/Sliders/SliderPartners/SliderPartners";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionSixBusiness.css";


const SectionSixBusiness = () => {
  return (
    <>
        <section className="section06_business">

            <div className="section06_business_title">
                <span className="section06_business_title_text_big">REJOIGNEZ LE MOUVEMENT</span>
                <span className="section06_business_title_text_small"><b> Nos clients entreprises : </b> Ils sont satisfaits par nos services</span>
                
            </div>
            <SliderPartners/>
            {/* <Partners/>           */}

            <div className="container_s6_businessBtn_container">
                <PrimaryBtn text={"Sollicitez une démo"} link={"#"}/>
            </div>
        </section>
    </>
  );
}

export default SectionSixBusiness;