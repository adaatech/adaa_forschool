import React from "react";

import "./style_oneGridItem.css";


const OneGridItem = ({one_id, one_img, one_title, one_content}) => {
  return (
    <>
    <div class="item">
        <div className="box-top-p">
            <img className="one_img" src={one_img} alt=""/>
        </div>
        <div className="box-bottom-p">
            <div className="box-bottom-title-p">{one_title}</div>
            <div className="box-bottom-content-p">{one_content}</div>
        </div>
    </div>
    </>
  );
}

export default OneGridItem;