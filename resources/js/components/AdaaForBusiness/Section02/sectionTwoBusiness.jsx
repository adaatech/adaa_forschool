import React from "react";

import OneGridItem from "./OneGridItemS2/oneGridItem";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionTwoBusiness.css";
import bg_picture from "../../../imgs/adaaForBusiness/laptop.jpg";


const SectionTwo = () => {
  return (
    <>
        <section className="section02">
            <img className="bg_picture_s2 " src={bg_picture} alt="background_picture"/>
            <div className="bg_color_layer_s2"></div>

            <div className="section02_title">
                <span className="section02_business_title_text_big">Avec +2000 formations dédiées aux entreprises,&nbsp;<u> Adaa For Business </u> &nbsp;est la solution e-learning idoine pour votre entreprise</span>
                {/* <span className="section02_title_text_small">pour vous apporter &nbsp;<u> l’infrastructure technologique e-learning </u> &nbsp; ainsi qu’un &nbsp; <u>catalogue large et riche de formations</u></span> */}
            </div>
            {/* <div className="section02_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}
            
            <div class="grid-container-section02 grid-container-section02-business">
                {

                    Sdata.adaaForSchoolS2Grid.map((val, ind) => {
                        return <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_title={val.title}
                                one_content={val.content}
                                />
                    })
                }

            </div>
        </section>
    </>
  );
}

export default SectionTwo;