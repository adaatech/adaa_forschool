import React from "react";

import SectionOne from "./Section01/sectionOneBusiness";
import SectionTwo from "./Section02/sectionTwoBusiness";
import SectionThree from "../Common/Categories/categories";
import SectionFour from "./Section04/sectionFourBusiness";
import SectionFive from "./Section05/sectionFiveBusiness";
import SectionSix from "./Section06/sectionSixBusiness";
import Formulaire from "../Common/Form/formulaire";

function AdaaForBusiness() {
  return (
      <>
    <SectionOne/>
    <SectionTwo/>
    <SectionThree/>
    <SectionFour/>
    <SectionFive/>
    <SectionSix/>
    <Formulaire/>
    </>
  );
}

export default AdaaForBusiness;