import {React} from "react";
import {Link} from "react-router-dom";

import SecondaryBtn from "../../Common/SecondaryBtn/secondaryBtn";
import "./style_sectionOneBusiness.css";

import bg_picture from "../../../imgs/adaaForBusiness/Image1.png";
import one_img from "../../../imgs/adaaForSchool/icon1.png";

function SectionOne() {
  return (
    <section className="section01_business">
        <img className="bg_picture_s1_business " src={bg_picture} alt="background_picture"/>
        
        <div className="teaser_s1_business_container">
            <div className="teaser_s1_business_img">
                <img className="one_img_business" src={one_img} alt=""/>
            </div>            
            <div className="teaser_s1_business_title">
                <span className="teaser_s1_business_title_text">DECUPLEZ LE POTENTIEL DE VOS EQUIPES TOUT EN REDUISANT VOS CHARGES DE FORMATION</span>                
            </div>            
            <div className="teaser_s1_businessBtn_container">
                <SecondaryBtn text={"Sollicitez une démo"} link={"#"}/>
            </div>
        </div>
    </section>
  );
}

export default SectionOne;