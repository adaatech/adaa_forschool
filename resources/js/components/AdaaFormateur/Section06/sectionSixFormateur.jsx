import React from "react";

import TextInput from "../../Common/InputOnly/TextInput/textInput";
import PhoneInput from "../../Common/InputOnly/PhoneInput/phoneInput";
import EmailInput from "../../Common/InputOnly/EmailInput/emailInput";
import TextArea from "../../Common/InputOnly/TextArea/textArea";
import SelectInput from "../../Common/InputOnly/SelectInput/selectInput";
import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";

import Sdata from '../../../sdata/Sdata';

import bg_picture from "../../../imgs/adaaFormateur/img1.PNG";

import "./style_sectionSixFormateur.css";


const SectionSixAmbassador = () => {
  return (
    <>
        <section className="section06_formateur">

            {/* <div className="section06_formateur_title">
                <span className="section06_formateur_title_text_big">Nous contacter</span>
                <span className="section06_formateur_title_text_small">Besoin de plus de détails ? Vous avez des questions ? N’hésitez pas à nous contacter. Nous nous ferons le plaisir de vous répondre dans les plus brefs délais :</span>
            </div> */}

            <div className="section06_formateur_container">
                <div class="grid-container-section06_formateur ">
                    <div class="itemLeft">
                        <div className="section06_formateur_title">
                            <span className="section06_formateur_title_text_big">FAITES CE QUE VOUS SAVEZ FAIRE LE MIEUX : FORMEZ</span>
                            <span className="section06_formateur_title_text_small">Nous nous occupons du reste. <br/>Remplissez  simplement  le  formulaire  et  laissez  notre  équipe  vous contacter, pour vous accompagner gratuitement à la préparation et création de vos premières formations.</span>
                        </div>
                        <div className="img_container_s6_formateur">
                            <img className="img_s6_formateur " src={bg_picture} alt="background_picture"/>
                        </div>
                    </div>
                    <div className="itemRight">
                    <form action="">
                        <div class="grid-container-itemRight-section06_formateur ">
                            <b className="section_title" >Informations personnelles</b>
                            <div className="grid-container-info_perso-section06_formateur">
                                <TextInput  nameText={"prenom"} grayColor={"True"} placeholderText={"Nom"} requiredValue={"True"}/>
                                <TextInput  nameText={"nom"} grayColor={"True"} placeholderText={"Prénom"} requiredValue={"True"}/>
                                <TextInput  nameText={"civilite"} grayColor={"True"} placeholderText={"Civilité"} requiredValue={"True"}/>
                                <EmailInput nameText={"email"} grayColor={"True"} placeholderText={"Email"} requiredValue={"True"}/>
                                <PhoneInput nameText={"telephone"} grayColor={"True"} placeholderText={"Téléphone"} requiredValue={"True"}/>
                                <PhoneInput nameText={"whatsapp"} grayColor={"True"} placeholderText={"Whatsapp"} requiredValue={"True"}/>                                
                                <TextInput  nameText={"pays"} grayColor={"True"} placeholderText={"Pays de résidence"} requiredValue={"True"}/>
                                <TextInput  nameText={"ville"} grayColor={"True"} placeholderText={"Ville de résidence"} requiredValue={"True"}/>
                            </div>

                            <b className="section_title" >Informations professionnelles</b>

                            <TextInput  nameText={"profession"} grayColor={"True"} placeholderText={"Profession"} requiredValue={"True"}/>
                            <SelectInput optionList={Sdata.selectList.votreCompetence} grayColor={"True"} title={"Quand souhaitez-vous avoir la démo"} nameText={"competence"} placeholderText={"votre(vos) domaine(s) de compétence"} requiredValue={"True"}/>
                            <SelectInput optionList={Sdata.selectList.votreNiveauCompetence} grayColor={"True"} title={"Quand souhaitez-vous avoir la démo"} nameText={"delaiDemo"} placeholderText={"votre niveau de compétence"} requiredValue={"True"}/>
                            
                            <span>Quelles sont vos idées de thèmes de formations ?</span>
                            <TextArea  rowsValue={"4"} grayColor={"True"} nameText={"nom"} placeholderText={"Thème(s)"} requiredValue={"True"}/>
                        </div>
                        <div className="submit_btn_container_s6_formateur">
                            <PrimaryBtn text={"Envoyer"} submit={"True"}/>
                        </div>
                    </form>
                    </div>
                </div>

            </div>

            
        </section>
    </>
  );
}

export default SectionSixAmbassador;