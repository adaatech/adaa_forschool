import {React} from "react";
import {Link} from "react-router-dom";

import SecondaryBtn from "../../Common/SecondaryBtn/secondaryBtn";
import "./style_sectionOneFormateur.css";

import bg_picture from "../../../imgs/adaaForBusiness/Image1.png";
import one_img from "../../../imgs/adaaForSchool/icon1.png";

function SectionOne() {
  return (
    <section className="section01_formateur">
        <img className="bg_picture_s1_formateur " src={bg_picture} alt="background_picture"/>
        
        <div className="teaser_s1_formateur_container">
            <div className="teaser_s1_formateur_img">
                <img className="one_img_business" src={one_img} alt=""/>
            </div>            
            <div className="teaser_s1_formateur_title">
                <span className="teaser_s1_formateur_title_text">
                    DEVENEZ UN FORMATEUR INTERNATIONAL
                    <span>
                    Partagez simplement votre expertise à des milliers de personnes dans le monde, transformez des vies et gagnez de l’argent.
                    </span>
                </span>                
            </div>            
            <div className="teaser_s1_formateurBtn_container">
                <SecondaryBtn text={"Devenez formateur"} link={"#"}/>
            </div>
        </div>
    </section>
  );
}

export default SectionOne;