import React from "react";

import SectionOne from "./Section01/sectionOneFormateur";
import TimelineFormateur from "../Common/Timelines/TimelineFormateur/Timeline";
import SectionTwo from "./Section02/sectionTwoFormateur";
import SectionThree from "./Section03/sectionThreeFormateur";
import SectionSix from "./Section06/sectionSixFormateur";
// import SectionThree from "../Common/Categories/categories";
import SectionFour from "../Common/Categories/categories";
// import SectionFive from "../Common/Form/formulaire";

function AdaaForBusiness() {
  return (
      <>
    <SectionOne/>
    <SectionTwo/>
    <SectionThree/>   
    <SectionFour/>
    <TimelineFormateur/>
    <SectionSix/>

    </>
  );
}

export default AdaaForBusiness;