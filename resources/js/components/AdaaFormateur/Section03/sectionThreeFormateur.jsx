import {React} from "react";
import {Link} from "react-router-dom";

import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";
import "./style_sectionThreeFormateur.css";

import bg_picture from "../../../imgs/adaaFormateur/Image16.jpg";
import icon1 from "../../../imgs/adaaFormateur/icon1.png";
import icon2 from "../../../imgs/adaaFormateur/icon2.png";
import icon3 from "../../../imgs/adaaFormateur/icon3.png";
import icon4 from "../../../imgs/adaaFormateur/icon4.png";
import icon5 from "../../../imgs/adaaFormateur/icon5.png";

function SectionThreeFormateur() {
  return (
    <section className="section03_formateur" >
        <img className="bg_picture_s3_formateur " src={bg_picture} alt="background_picture"/>
        <div className="teaser_s3_formateur">
            <div className="teaser_s3_formateur_container">               
                <div className="teaser_s3_formateur_title">
                    <span className="teaser_s3_formateur_title_text">05 RAISONS DE DEVENIR FORMATEUR ADAA</span>             
                </div>
                {/* <div className="teaser_s3_formateur_content">                
                    <p>Rendons ceci possible en quelques étapes simples et pratiques :</p>
                </div> */}
                <div class="grid-container-section03-formateur ">
                    <div class="item_s3_formateur">
                        <div className="box-left-s3_formateur">
                            <img className="one_img" src={icon1} alt=""/>
                        </div>
                        <div className="box-right-s3_formateur">
                            <span className="box-right-title-s3_formateur">PARTAGEZ VOS EXPERTISES, IMPACTEZ DES VIES</span>
                            <span className="box-right-content-s3_formateur">Contribuez  à  construire  et  impacter  positivement  l’humanité  en formant des hommes et femmes sur toute la planète</span>
                        </div>
                    </div>
                    <div class="item_s3_formateur">
                        <div className="box-left-s3_formateur">
                            <img className="one_img" src={icon2} alt=""/>
                        </div>
                        <div className="box-right-s3_formateur">
                            <span className="box-right-title-s3_formateur">GAGNEZ DE L’ARGENT</span>
                            <span className="box-right-content-s3_formateur">Créer  et  publier  vos  formations,  nous  nous  chargeons  du  reste  : promotion,  assistance  des  potentiels  apprenants,  suivi  et  analyse  de l’efficacité de vos cours, etc.</span>
                        </div>
                    </div>
                    <div class="item_s3_formateur">
                        <div className="box-left-s3_formateur">
                            <img className="one_img" src={icon3} alt=""/>
                        </div>
                        <div className="box-right-s3_formateur">
                            <span className="box-right-title-s3_formateur">DISPOSEZ DES MEILLEURS OUTILS POUR CRÉER ET GERER VOS FORMATIONS</span>
                            <span className="box-right-content-s3_formateur">Nous  mettons  à  votre  disposition  les  meilleures  interfaces  et  outils technologiques ainsi que l’assistance 24h/24 et 7j/7 pour créer et gérer au quotidien vos formations.</span>
                        </div>
                    </div>
                    <div class="item_s3_formateur">
                        <div className="box-left-s3_formateur">
                            <img className="one_img" src={icon4} alt=""/>
                        </div>
                        <div className="box-right-s3_formateur">
                            <span className="box-right-title-s3_formateur">IMPOSEZ VOUS COMME UN EXPERT DE VOTRE DOMAINE ET INTEGREZ LA COMMUNAUTE DES FORMATEURS</span>
                            <span className="box-right-content-s3_formateur">Impactez  des  milliers  d’apprenants  à  travers  le  monde  et  consolidez sur  l’international  votre  statut  d’expert  dans  votre  domaine  de compétence</span>
                        </div>
                    </div>
                    <div class="item_s3_formateur">
                        <div className="box-left-s3_formateur">
                            <img className="one_img" src={icon5} alt=""/>
                        </div>
                        <div className="box-right-s3_formateur">
                            <span className="box-right-title-s3_formateur">ACCEDEZ A UN TABLEAU DE BORDS TRANSPARENT DE VOS REVENUS</span>
                            <span className="box-right-content-s3_formateur">Nous  mettons  à  votre  disposition  un  tableau  de  bords  vous permettant  d’apprécier  en  temps  réel  votre  activité  formateurs (apprenants, revenus, paiement,...)</span>
                        </div>
                    </div>
                </div>

                <div className="s3_formateur_btn_container">
                    <PrimaryBtn text={"Devenez formateur"} submit={"True"}/>
                </div>
            </div>
        </div>
    </section>
  );
}

export default SectionThreeFormateur;