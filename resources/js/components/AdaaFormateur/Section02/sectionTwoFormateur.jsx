import {React} from "react";
import {Link} from "react-router-dom";

import SliderFormateur from "../../Common/Sliders/SliderFormateur/SliderFormateur";


import TernaryBtn from "../../Common/TernaryBtn/ternaryBtn";
import "./style_sectionTwoFormateur.css";

import bg_picture_1 from "../../../imgs/adaaForInvest/Image7.png";
import bg_picture_2 from "../../../imgs/adaaForBusiness/Image2.png";
import one_img from "../../../imgs/adaaForSchool/icon5.png";
import video from '../../../videos/video_S9_invest.mp4'

function SectionTwoFormateur() {
  return (
    <section className="section02_formateur">
    <div className="section02_formateur_container_one">
        <div className="section02_formateur_title">
            <span className="section02_formateur_title_text_big">REJOIGNEZ LA PLUS GRANDE COMMUNAUTE D’EXPERTS FORMATEURS DU CONTINENT</span>
            <span className="section02_formateur_title_text_small">Profitez de notre technologie et actions marketing pour monétiser vos compétences à travers des formations conçues et gérées par vous.</span>
        </div>
        
        <div className="container_s2_formateur">
            <div className="boxLeft_s2_formateur">                
                <video width="100%" height="auto" controls >
                    <source src={video} type="video/mp4"/>
                </video>
            </div> 
            <div className="boxRight_s2_formateur">            
                <div className="section02_formateur_right_top_grid">
                    <div className="section02_formateur_right_top_left">
                        <div className="box-top_formateur"> 
                            <div className="perso_formateur" style={{background: "url("+ one_img + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}></div>                                
                        </div>
                        <div className="box-bottom_formateur">
                            <div className="box-bottom-title_formateur">Nicolas NIAT</div>
                            <div className="box-bottom-content_formateur">Formateur</div>
                        </div>
                        </div>
                    <div className="section02_formateur_right_top_right">
                        <span><i class="material-icons">star</i><b>4.6</b>Note du formateur</span>
                        <span><i class="material-icons">chat</i><b>17 895</b>Avis</span>
                        <span><i class="material-icons">person</i><b>333 777</b>Participants</span>
                        <span><i class="material-icons">play_circle_filled</i><b>35</b>Cours</span>
                    </div>                    
                    
                    {/* <span className="section02_formateur_title_text_small">Poursuivre notre expansion africaine </span> */}
                </div>                   
                <div className="section02_formateur_right_bottom">
                    <div className="section02_formateur_right_bottom_title">Poursuivre notre expansion africaine</div>
                    <div className="section02_formateur_right_bottom_content">
                    Avec une option de formations présentielles pour entreprises, cadres et chercheurs d’emplois dans les domaines dans un cadre confortable
                    </div>
                    <a href="#" className="section02_formateur_right_bottom_link">+ Voir plus</a>
                
                
                </div>
            
                {/* <div className="boxRight_s2_formateurBtn_container">
                    <TernaryBtn text={"Devenez investisseur"} link={"#"}/>
                </div> */}
            </div>
                      
        </div>
        </div>
        <div className="section02_formateur_container_two">
            <div className="section02_formateur_title">
                <span className="section02_formateur_title_text_big">COMME EUX, CONSTRUISEZ VOTRE COMMUNAUTÉ D'APPRENANTS A TRAVERS LE MONDE</span>                
            </div>
            <SliderFormateur/>
        </div>        
    </section>
  );
}

export default SectionTwoFormateur;