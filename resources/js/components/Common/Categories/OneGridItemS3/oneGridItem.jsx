import React from "react";

import "./style_oneGridItem.css";


const OneGridItem = ({one_id, one_img, one_title, one_content, one_ind, categoriesOpen, setCategoriesOpen}) => {
    
  return (
    <>
    <div class={`item ${(one_ind>11 && categoriesOpen==false) ? "displayNone": ""}`}>
        <div className="box-left-categories">
            <img className="one_img" src={one_img} alt=""/>
        </div>
        <div className="box-right-categories">
            {one_title}
        </div>
    </div>
    </>
  );
}

export default OneGridItem;