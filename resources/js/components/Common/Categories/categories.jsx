import {React, useState} from "react";

import OneGridItem from "./OneGridItemS3/oneGridItem";

import "./style_Categories.css";

import icon_1 from './categories/big-data.png';
import icon_2 from './categories/blockchain.png';
import icon_3 from './categories/business-intelligence.png';
import icon_4 from './categories/cyber-security.png';
import icon_5 from './categories/design-graphisme.png';
import icon_6 from './categories/app.png';
import icon_7 from './categories/icone_developpement_personel.png';
import icon_8 from './categories/developpement-web.png';
import icon_9 from './categories/drone.png';
import icon_10 from './categories/entrepreneuriat.png';
import icon_11 from './categories/icone finance et compta 2.png';
import icon_12 from './categories/database.png';
import icon_13 from './categories/gestion-des-projets.png';
import icon_14 from './categories/icone informatique.png';
import icon_15 from './categories/inno.png';
import icon_16 from './categories/responsive-design.png';
import icon_17 from './categories/brain.png';
import icon_18 from './categories/progress.png';
import icon_19 from './categories/download.png';
import icon_20 from './categories/icone Marketing et vente.png';
import icon_21 from './categories/id-card.png';
import icon_22 from './categories/photography.png';
import icon_23 from './categories/crm.png';
import icon_24 from './categories/broadcast.png';
import icon_25 from './categories/dollar.png';
// import icon_26 from './categories/photography.png';
// import icon_27 from './categories/photography.png';



var categories = [
    {
        id: 1,
        name: 'Big-data',
        img: icon_1
    },
    {
        id: 2,
        name: 'BlockChain',
        img: icon_2
    },
    {
        id: 3,
        name: 'Business Intelligence',
        img: icon_3
    },
    {
        id: 4,
        name: 'CyberSecurité',
        img: icon_4
    },
    {
        id: 5,
        name: 'Design et Graphisme',
        img: icon_5
    },
    {
        id: 6,
        name: 'Développement mobile',
        img: icon_6
    },
    {
        id: 7,
        name: 'Développement personnel',
        img: icon_7
    },
    {
        id: 8,
        name: 'Développement web',
        img: icon_8
    },
    {
        id: 9,
        name: 'Drone',
        img: icon_9
    },
    {
        id: 10,
        name: 'Entrepreneuriat',
        img: icon_10
    },
    {
        id: 11,
        name: 'Finance et comptabilité',
        img: icon_11
    },
    {
        id: 12,
        name: 'Gestion des bases de données',
        img: icon_12
    },
    {
        id: 13,
        name: 'Gestion des projets',
        img: icon_13
    },
    {
        id: 14,
        name: 'Informatique',
        img: icon_14
    },
    {
        id: 15,
        name: 'Innovation et Productivité',
        img: icon_15
    },
    {
        id: 16,
        name: 'Intégration web et mobile',
        img: icon_16
    },
    {
        id: 17,
        name: 'Intélligence artificielle',
        img: icon_17
    },
    {
        id: 18,
        name: 'IoT',
        img: icon_18
    },
    {
        id: 19,
        name: 'Logiciel & ERP',
        img: icon_19
    },
    {
        id: 20,
        name: 'Marketing et vente',
        img: icon_20
    },
    {
        id: 21,
        name: 'Personal Branding',
        img: icon_21
    },
    {
        id: 22,
        name: 'Photographie',
        img: icon_22
    },
    {
        id: 23,
        name: 'Rélation client',
        img: icon_23
    },
    {
        id: 24,
        name: 'Réseaux & Télecoms',
        img: icon_24
    },
    {
        id: 25,
        name: 'Trading',
        img: icon_25
    }
]

const Categories = () => {
    const [categoriesOpen, setCategoriesOpen] = useState(false);

    const handleCategoriesOpen = () => {
        categoriesOpen ?
        setCategoriesOpen(false)
        :
        setCategoriesOpen(true)
    }

  return (
    <>
        <section className={`categories ${(categoriesOpen) ? "heightFull":""}`}>
            <div className="categories_container">            
                <div className="categories_title">
                    <span className="categories_title_text_big">Construisons et personnalisons ensemble</span>
                    <span className="categories_title_text_small">votre catalogue de formations dans plus de 27 Catégories</span>
                </div>
                
                <div class="grid-container-categories ">
                    {

                        categories.map((val, ind) => {
                            return <OneGridItem
                                    key={ind}
                                    one_id={val.id}
                                    one_img={val.img}
                                    one_title={val.name}
                                    one_ind={ind}
                                    categoriesOpen={categoriesOpen}
                                    setCategoriesOpen={setCategoriesOpen}
                                    />
                        })
                    }

                </div>

                <div className="categories_btn_container">
                    <div onClick={handleCategoriesOpen} className={`categories_voir_plus_btn ${categoriesOpen?"displayNone":""}`}>{categoriesOpen?"- Voir moins":"+ Voir plus"}</div>
                </div>
            </div>
        </section>
    </>
  );
}

export default Categories;