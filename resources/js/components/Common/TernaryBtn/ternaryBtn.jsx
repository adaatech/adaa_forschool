import {React} from "react";
import {Link} from "react-router-dom";


import "./style_ternaryBtn.css";

function TernaryBtn({text, link, submit}) {
  return (
    
    submit ?
        <input className="oneTernaryBtn" type="submit" value={text}/>
    :
        <Link className="oneTernaryBtn" to={link}>
            <span>{text}</span>
        </Link>
        
    
  );
}

export default TernaryBtn;