import {React} from "react";
import {Link} from "react-router-dom";

import PrimaryBtn from "../../../Common/PrimaryBtn/primaryBtn";
import "./style_timeline.css";

// import bg_picture from "../../../../imgs/adaaForBusiness/Image1.png";
import one_img_1 from "../../../../imgs/adaaFormateur/Image1.png";
import one_img_2 from "../../../../imgs/adaaFormateur/Image2.png";
import one_img_3 from "../../../../imgs/adaaFormateur/Image3.png";
import one_img_4 from "../../../../imgs/adaaFormateur/Image4.png";
import one_img_5 from "../../../../imgs/adaaFormateur/Image5.png";

function SectionOne() {
  return (
    <section className="timeline_formateur">
        <div className="timeline_title">
            <span className="timeline_title_text_big">Comment ça marche ?</span>
            <span className="timeline_title_text_small">Nous vous accompagnons dans tout le process.<br/>Nos équipes sont disponibles 7j7, 24h/24 à vous apporter l’assistance dont vous aurez besoin.</span>
        </div>
        <div className="row">
            <div className="experience tab-content">
                <div className="row">
                    <div className="timeline">
                        <div className="row">
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="fa fa-briefcase icon"></i> */}
                                    {/* <i class="material-icons icon">work</i> */}
                                    <span class="icon" style={{background: "url("+ one_img_1 + ") center center no-repeat", backgroundSize: "80% auto", backgroundPosition: "center"}}></span>
                                    <span className="itemNumber">1</span>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Créez votre compte Adaa et planifiez vos formations</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                    Inscrivez-vous en  remplissant simplement le formulaire <a href="#">s’inscrire</a> 
                                    <br/><br/>
                                    Identifiez vos domaines de compétences qui vous passionnent  et pour lesquels vous souhaitez créer des formations. Ensuite, planifiez la réalisation de vos  cours.
                                    <br/><br/>
                                    N’hésitez pas à contacter au besoin l’équipe Adaa qui vous donnera quelques astuces
                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="briefcase icon"></i> */}
                                    {/* <i class="material-icons icon">work</i> */}
                                    <span class="icon" style={{background: "url("+ one_img_2 + ") center center no-repeat", backgroundSize: "80% auto", backgroundPosition: "center"}}></span>
                                    <span className="itemNumber">2</span>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Enregistrez vos formations</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                        Votre formation vous pouvez l’enregistrer à la maison, au bureau ou tout autre lieu calme, convivial et adapté pour la production d’un contenu de qualité.
                                        <br/><br/>
                                        Commençons par le commencement : trouvez un appareil photo numérique ou utilisez votre smartphone et aussi un  microphone pour capture le son.
                                        <br/><br/>
                                        Pour l’enregistrement des vidéos de démonstration et le montage de votre vidéo de formation, nous vous recommandons le logiciel Camtasia. 
                                        <br/><br/>
                                        Regardez la <a href="#">démo</a> 
                                        <br/><br/>
                                        N’oubliez pas nos supers Adaaiens  du service support sont disponibles 7j7, 24h/24 à vous apporter
                                        l’assistance dont vous aurez besoin

                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="fa fa-briefcase icon"></i> */}
                                    <span className="itemNumber">3</span>
                                    {/* <i class="material-icons icon">work</i> */}
                                    <span class="icon" style={{background: "url("+ one_img_3 + ") center center no-repeat", backgroundSize: "80% auto", backgroundPosition: "center"}}></span>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Publiez vos formations sur Adaa</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                    <a href="#">Connectez-vous</a> à votre espace formateur sur  Adaa et publiez vos formations.
                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="fa fa-briefcase icon"></i> */}
                                    <span className="itemNumber">4</span>
                                    {/* <i class="material-icons icon">work</i> */}
                                    <span class="icon" style={{background: "url("+ one_img_4 + ") center center no-repeat", backgroundSize: "80% auto", backgroundPosition: "center"}}></span>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Bénéficiez des actions de promotion d’Adaa pour vos formations</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                    Vous créons le trafic sur notre plateforme et vers vos formations (à travers les moteurs de recherche, les réseaux sociaux, l’affiliation, etc.) afin de vous permettre de recruter des apprenants pour vos formations.
                                    <br/><br/>
                                    Nous vous encourageons à promouvoir autant que possible vos formations dans vos réseaux respectifs (réseaux sociaux, communautés d’amis, de collègues, etc.)
                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="fa fa-briefcase icon"></i> */}
                                    <span className="itemNumber">5</span>
                                    {/* <i class="material-icons icon">work</i> */}
                                    <span class="icon" style={{background: "url("+ one_img_5 + ") center center no-repeat", backgroundSize: "80% auto", backgroundPosition: "center"}}></span>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Gagnez de l’argent sur chaque achat à chaque commande de l’une de vos formations</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                    Mieux le nombre de vos apprenants sera important, mieux vos gains seront importants. Gagnez de l'argent pour chaque commande de formation confirmée et payée.
                                    <br/>
                                    <a href="#">Découvrez la politique Adaa de rémunération des formateurs</a>                                   
                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                        </div>
                    </div>
                </div>
                <div className="timelineBtn_container">
                    <PrimaryBtn text={"Dévenir formateur"} link={"#"}/>
                </div>
            </div>
        </div>
    </section>
  );
}

export default SectionOne;