import {React} from "react";
import {Link} from "react-router-dom";

import PrimaryBtn from "../../../Common/PrimaryBtn/primaryBtn";
import "./style_timeline.css";

import bg_picture from "../../../../imgs/adaaForBusiness/Image1.png";
import one_img from "../../../../imgs/adaaForSchool/icon1.png";

function SectionOne() {
  return (
    <section className="timeline_ambassador">
        <div className="timeline_title">
            <span className="timeline_title_text_big">COMMENT CA MARCHE ?</span>
            {/* <span className="timeline_title_text_small">(Avril 2022 - Avril 2025)</span> */}
        </div>

        <div className="row">
            <div className="experience tab-content">
                <div className="row">
                    <div className="timeline">
                        <div className="row">
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="fa fa-briefcase icon"></i> */}
                                    <i class="material-icons icon">computer</i>
                                    {/* <span class="icon"></span> */}
                                    <span className="itemNumber">1</span>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Crééz votre compte</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                    Créez votre compte BRAND AMBASSADOR, <a href="#">téléchargez l’application mobile Adaa Learning</a> et attendez qu’il soit validé par l’équipe ADAA
                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="briefcase icon"></i> */}
                                    <i class="material-icons icon">groups</i>
                                    <span className="itemNumber">2</span>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Adaa vous forme</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                    L’équipe ADAA vous forme et valide votre compte Ambassadeur, après quoi vous recevez un CODE BRAND AMBASSADOR
                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="fa fa-briefcase icon"></i> */}
                                    <span className="itemNumber">3</span>
                                    <i class="material-icons icon">hearing</i>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Recrutez des clients</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                    Parler de ADAA autour de vous et recrutez des clients autour de vous et utilisez ç chaque fois votre CODE BRAND AMBASSADOR pour leurs offrir des réductions significatives
                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                            {/* timeline-item start */}
                            <div className="timeline-item">
                                <div className="timeline-item-inner">
                                    {/* <i className="fa fa-briefcase icon"></i> */}
                                    <span className="itemNumber">4</span>
                                    <i class="material-icons icon">phone_iphone</i>
                                    {/* <span>Sep, 2021</span> */}
                                    <h3>Gagnez de l’argent et des formations gratuites</h3>
                                    {/* <h4>Company name, india</h4> */}
                                    <p>
                                    Suivez en temps réel via l’application mobile et le site <a href="#">www.adaalearning.com</a> vos réalisations, vos revenus, les formations gratuites obtenues, etc.
                                    </p>
                                </div>
                            </div>
                            {/* timeline-item end */}
                        </div>
                    </div>
                </div>
                <div className="timelineBtn_container">
                    <PrimaryBtn text={"Dévenir ambassadeur"} link={"#"}/>
                </div>
            </div>
        </div>
    </section>
  );
}

export default SectionOne;