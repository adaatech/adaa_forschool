import {React} from "react";
/* import { Link } from "react-router-dom"; */


import "./style_primaryBtn.css";

function PrimaryBtn({text, link, submit}) {
  return (
    
    submit ?
        <input className="onePrimaryBtn" type="submit" value={text}/>
    :
        <a className="onePrimaryBtn btn-primary" href={link}>
            <span>{text}</span>
        </a>
        
    
  );
}

export default PrimaryBtn;