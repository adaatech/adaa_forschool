import React from "react";

import TextInput from "../../Common/InputAndTitle/TextInput/textInput";
import SelectInput from "../../Common/InputAndTitle/SelectInput/selectInput";
import SelectCountry from "../../Common/InputAndTitle/SelectCountry/selectCountry";
import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";

import "./style_formulaire.css";


const Formulaire = () => {

    const selectList = {
        votreFonction: [
            { value: "presidentFondateur", text: "Président/Fondateur" },
            { value: "recteur", text: "Recteur" },
            { value: "viceRecteur", text: "Vice-recteur" },
            { value: "directeur", text: "Directeur" },
            { value: "chefDépartement", text: "Chef de département" },
            { value: "enseignant", text: "Enseignant" },
            { value: "ceo", text: "CEO" },
            { value: "coo-cfo-cio", text: "COO/CFO/CIO" },
            { value: "autres", text: "Autres" }
        ],
        categorieInstitution: [
            { value: "universiteEcolePublique", text: "Université / Ecole publique" },
            { value: "universiteEcolePrivée", text: "Université / Ecole privée" },
            { value: "gouvernement", text: "Gouvernement" },
            { value: "ong", text: "ONG (Organisation Non Gouvernementale)" },
            { value: "institutionPubliqueEmploi", text: "Institution publique pour l'emploi" },
            { value: "startupElearning", text: "Start-up e-learning" },
            { value: "autres", text: "Autres" }
        ],
        effectifPersonnes: [
            { value: "v100-1000personnes", text: "100 - 1000 personnes" },
            { value: "v1001-5000personnes", text: "1001 - 5000 personnes" },
            { value: "v5001-10000personnes", text: "5001 - 10 000 personnes" },
            { value: "v10000-50000personnes", text: "10 000 - 50 000 personnes" },
            { value: "v50000personnes", text: "+50 000 personnes" }
        ],
        delaiDemo: [
            { value: "lePlusTôtPossible", text: "Le plus tôt possible" },
            { value: "dans3a7jours", text: "Dans 3 à 7 jours" },
            { value: "dans1semaine", text: "Dans 1 semaine" },
            { value: "dans1mois", text: "Dans 1 mois" },
            { value: "autres", text: "Autres" }
        ],
        votreCompetence: [
            { value: "mecatronique", text: "Mecatronique" },
            { value: "devweb", text: "Developpement Web" },
            { value: "commdigital", text: "Communication Digitale" },
        ],
        votreNiveauCompetence: [
            { value: "bacc", text: "BACC" },
            { value: "bts", text: "BTS" },
            { value: "licence", text: "Licence" },
            { value: "master", text: "Master" },
        ],
    }

    return (
        <>
            <section className="adaaFormulaire">

                <div className="bg_color_layer_s2"></div>

                <div className="adaaFormulaire_title">
                    <span className="adaaFormulaire_title_text_big">Votre transformation commence ici</span>
                    <span className="adaaFormulaire_title_text_small">Sollicitez une démo détaillée en remplissant le formulaire ci-dessous.</span>
                </div>
                {/* <div className="adaaFormulaire_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}
                <form action="">
                    <div class="grid-container-adaaFormulaire ">
                        <TextInput title={"Nom de votre institution"} nameText={"nomInstitution"} placeholderText={"Ex.: Université Adaa"} requiredValue={"True"} />
                        <SelectInput optionList={selectList.categorieInstitution} title={"Catégorie de votre institution"} nameText={"categorieInstitution"} placeholderText={"votre catégorie"} requiredValue={"True"} />
                        <TextInput title={"Prénom(s)"} nameText={"prenom"} placeholderText={"Votre/Vos prénom(s) ..."} requiredValue={"True"} />
                        <TextInput title={"Nom(s)"} nameText={"nom"} placeholderText={"Votre/Vos nom(s) ..."} requiredValue={"True"} />
                        <SelectInput optionList={selectList.votreFonction} title={"Votre fonction"} nameText={"fonction"} placeholderText={"votre fonction"} requiredValue={"True"} />
                        <SelectCountry requiredValue={"True"} />
                        <TextInput title={"Téléphone"} nameText={"telephone"} placeholderText={"Ex.: (+237) 6xx xxx xxx"} requiredValue={"True"} />
                        <TextInput title={"Email"} nameText={"email"} placeholderText={"Ex.: exemple@email.com"} requiredValue={"True"} />
                        <SelectInput optionList={selectList.effectifPersonnes} title={"Effectifs des personnes à former en e-learning"} nameText={"effectifPersonnes"} placeholderText={"votre effectif"} requiredValue={"True"} />
                        <SelectInput optionList={selectList.delaiDemo} title={"Quand souhaitez-vous avoir la démo"} nameText={"delaiDemo"} placeholderText={"votre délai"} requiredValue={"True"} />

                    </div>
                    <div className="submit_btn_container">
                        <PrimaryBtn text={"Soumettre"} submit={"True"} />
                    </div>
                </form>
            </section>
        </>
    );
}

export default Formulaire;