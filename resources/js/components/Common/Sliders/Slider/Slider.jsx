import React from "react";

import bg_picture_1 from "../../../../imgs/adaaForInvest/Image5.jpg";

import "./style_slider.css";

const items = [
    { title: "Image 0", image: "image0" },
    { title: "Image 1", image: "image1" },
    { title: "Image 2", image: "image2" },
    { title: "Image 3", image: "image3" },
    { title: "Image 4", image: "image4" },
    { title: "Image 5", image: "image5" },
    { title: "Image 6", image: "image6" },
    { title: "Image 7", image: "image7" }
  ];

const ItemComponent = ({ title }) => {
    // return <div className="ItemComponent">{title}</div>;
    return (
        <div className="ItemComponent card-container">
            
            <div className="card" style={{background: "url("+ bg_picture_1 + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}>
            {/* <img className="bg_picture_card_invest " src={bg_picture_1} alt="background_picture"/> */}
            <div className="img"></div>
            {/* <div className="info">
                Title 1
            </div> */}
            </div>
        </div>
    )
  };

const Slider = () => {
    const [clickCount, setClickCount] = React.useState(0);
    const mainWrap = React.useRef();
    const containerRef = React.useRef();

    const scrollSllides = (direction) => {
        const width = mainWrap.current?.clientWidth;
        console.log("mainWrap", mainWrap.current?.offsetWidth)
        // console.log("containerRef", containerRef.current?.offsetWidth)
        let scrollTo;
        let stepCardnumber = 3;
        const tablette = width <= 720 
        const mobile = width <= 320 
        if (tablette) {
            stepCardnumber = 2;
        }
        else if (mobile) {
            stepCardnumber = 1;
        }
        const diff = direction === "next" ? 1 : -1;
        const newValue = (clickCount + diff) % (items.length / stepCardnumber);
        
        setClickCount(newValue);
        scrollTo = width * newValue;
        console.log(newValue)
        console.log(scrollTo)

        containerRef.current?.scrollTo({
            behavior: "smooth",
            left: scrollTo
        });
    }
  return (
    <>
        <div ref={mainWrap}>
      {/* <button onClick={() => scrollSllides("prev")}>Prev</button>
      <button onClick={() => scrollSllides("next")}>Next</button> */}
      
      <div className="Slider carousel-container" >
        <div className="nav">
            <button className={`prev ${clickCount>0 ? "show":""}`} onClick={() => scrollSllides("prev")}>
                <i class="material-icons">arrow_left</i>
            </button>
            <button className="next" onClick={() => scrollSllides("next")}>
                <i class="material-icons">arrow_right</i>
            </button>         
        </div>
        <div className="carousel-inner" ref={containerRef}>
            <div className="track">
                {items?.map((item, index) => (
                <ItemComponent key={index} title={item.title} />
                ))}
            </div>
        </div>
        
      </div>
    </div>
    </>
  );
}

export default Slider;