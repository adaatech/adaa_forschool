import {React, useState, useEffect, useRef} from "react";

import bg_picture_1 from "../../../../imgs/adaaForInvest/Image5.jpg";

import "./style_sliderTraining.css";

import image1 from "../../../../imgs/adaaForInvest/section_10_training/img-training1.png";
import image2 from "../../../../imgs/adaaForInvest/section_10_training/img-training2.png";
import image3 from "../../../../imgs/adaaForInvest/section_10_training/img-training3.png";
import image4 from "../../../../imgs/adaaForInvest/section_10_training/img-training4.png";

const items = [
    { img: image1 },
    { img: image2 },
    { img: image3 },
    { img: image4 }
  ];

const ItemComponent = ({ img }) => {
    // return <div className="ItemComponent">{title}</div>;
    return (
        <div className="ItemComponent card_container_training">
            
            <div className="card_training" style={{background: "url("+ img + ") center center no-repeat", backgroundSize: "auto 100%", backgroundPosition: "center"}}>
            {/* <img className="bg_picture_card_invest " src={bg_picture_1} alt="background_picture"/> */}
            <div className="img"></div>
            {/* <div className="info">
                Title 1
            </div> */}
            </div>
        </div>
    )
  };

const Slider = () => {
    const [clickCount, setClickCount] = useState(0);
    const [hideRightArrow, setHideRightArrow] = useState(false);
    const [fitSlider, setFitSlider] = useState(false);

    const mainWrap = useRef();
    const containerRef = useRef();

    useEffect(() => {
        const width = mainWrap.current?.clientWidth;
        let stepCardnumber = 3;
        const tablette = (width <= 720 && width > 340)
          const mobile = width <= 340 
          if (tablette) {
              stepCardnumber = 2;
          }
          else if (mobile) {
              stepCardnumber = 1;
          }
  
        //   console.log("",stepCardnumber)
        //   console.log(items.length)
        if (items.length <= stepCardnumber) {
          setHideRightArrow(true);
          setFitSlider(true);
        } else {
          setHideRightArrow(false);
          setFitSlider(false);
        }
      }, [])

    const scrollSllides = (direction) => {
        const width = mainWrap.current?.clientWidth;
        // console.log("mainWrap", mainWrap.current?.offsetWidth)
        // console.log("containerRef", containerRef.current?.offsetWidth)
        let scrollTo;
        let stepCardnumber = 3;
        const tablette = (width <= 720 && width > 340)
        const mobile = width <= 340 
        if (tablette) {
            stepCardnumber = 2;
        }
        else if (mobile) {
            stepCardnumber = 1;
        }
        const diff = direction === "next" ? 1 : -1;
        const newValue = (clickCount + diff) % (items.length / stepCardnumber);
        // console.log(newValue);
        setClickCount(newValue);
        scrollTo = width * newValue;
        
        // handleRightArrow
        if (((newValue+1)*stepCardnumber) >= items.length) {
            setHideRightArrow(true);
          } else {
            setHideRightArrow(false);
          }

        containerRef.current?.scrollTo({
            behavior: "smooth",
            left: scrollTo
        });
    }
  return (
    <>
        <div ref={mainWrap}>
      {/* <button onClick={() => scrollSllides("prev")}>Prev</button>
      <button onClick={() => scrollSllides("next")}>Next</button> */}
      
      <div className="Slider carousel-container-training" >
        <div className="nav">
            <button className={`prev ${clickCount>0 ? "show":""}`} onClick={() => scrollSllides("prev")}>
                <i class="material-icons">arrow_left</i>
            </button>
            <button className={`next ${hideRightArrow ? "hide":""} `} onClick={() => scrollSllides("next")}>
                <i class="material-icons">arrow_right</i>
            </button>         
        </div>
        <div className="carousel-inner-training" ref={containerRef}>
            <div className="track">
                {items?.map((item, index) => (
                <ItemComponent 
                key={index} 
                img={item.img}               
                />
                ))}
            </div>
        </div>
        
      </div>
    </div>
    </>
  );
}

export default Slider;