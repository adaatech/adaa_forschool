import {React, useState, useEffect, useRef} from "react";

import love_investor_1 from "../../../../imgs/adaaForInvest/007.png";

import "./style_slideShow.css";

const items = [
    { 
      one_title: "Février 2019", 
      one_content: "Alors composé d’une équipe de 03 personnes, nous lançons notre 1ére formation présentielle avec location d’une salle de formation", 
    //   one_img: bg_picture_1
    },
    { 
      one_title: "Juillet 2019", 
      one_content: "Intégration de notre 2éme bureau (plus grand) qui faisait office de salle de formation", 
    //   one_img: bg_picture_2
    }, 
    { 
      one_title: "Mars 2020",
      one_content: "Alors composé d’une équipe de 03 personnes, nous lançons notre 1ére formation présentielle avec location d’une salle de formation", 
    //   one_img: bg_picture_1
    },
    { 
      one_title: "Avril 2020", 
      one_content: "Intégration de notre 2éme bureau (plus grand) qui faisait office de salle de formation", 
    //   one_img: bg_picture_2
    },
  ];

const ItemComponent = ({ one_title, one_content, one_img }) => {
    // return <div className="ItemComponent">{title}</div>;
    return (
        <div className="ItemComponent card-container-slide_loveInvestors">
            <div className="card-slide_loveInvestors">
                <div className="boxLeft_slide_loveInvestors">                    
                    <div className="boxLeft_text_slide_loveInvestors">
                    « Mon premier contact avec l’équipe 
ADAA en 2019 a été très chaleureux. J’ai découvert avec grand étonnement ce que des africains étaient entrain de bâtir. C’est pourquoi je n’avais pas du tout hésiter à rejoindre le projet comme Investisseur de la première heure. Les assemblées générales et les rapports trimestrielles, les newsletters mensuelles me permette à titre d’investisseur depuis 1 an et demi d’avoir une vue claire de l’évolution de l’activité. J’ai foi en ce projet et en son équipe. »

                    </div>
                    <div className="boxLeft_text_author_slide_loveInvestors">
                        
                            Mermoz KOUASSI
                            <span>DG, PERFORMANCES RH</span>
                            Abidjan – Côte d’Ivoire
                        
                    </div>
                </div>
                <div className="boxRight_slide_loveInvestors">
                    <img className="bg_picture_slide_loveInvestors " src={love_investor_1} alt="background_picture"/>                
                </div>           
            </div>
            {/* <div className="card-slide_loveInvestors">
                <div className="box-top-slide_loveInvestors">
                    <div className="box-top-title-slide_loveInvestors">{one_title}</div>
                    <div className="box-top-content-slide_loveInvestors">{one_content}</div>
                </div>
                <div className="box-bottom-slide_loveInvestors"> 
                <img className="bg_picture_card_invest " src={one_img} alt="background_picture"/>
                    
                </div>
            </div> */}
        </div>
    )
  };

const Slider = () => {
    const [clickCount, setClickCount] = useState(0);
    const [hideRightArrow, setHideRightArrow] = useState(false);
    const [fitSlider, setFitSlider] = useState(false);

    const mainWrap = useRef();
    const containerRef = useRef();

    useEffect(() => {
        const width = mainWrap.current?.clientWidth;
        let stepCardnumber = 1;
        const tablette = (width <= 640 && width > 340)
          const mobile = width <= 340 
          if (tablette) {
              stepCardnumber = 1;
          }
          else if (mobile) {
              stepCardnumber = 1;
          }
  
        //   console.log("",stepCardnumber)
        //   console.log(items.length)
        if (items.length <= stepCardnumber) {
          setHideRightArrow(true);
          setFitSlider(true);
        } else {
          setHideRightArrow(false);
          setFitSlider(false);
        }
      }, [])

    const scrollSllides = (direction) => {
        const width = mainWrap.current?.clientWidth;
        console.log("mainWrap", mainWrap.current?.offsetWidth)
        // console.log("containerRef", containerRef.current?.offsetWidth)
        let scrollTo;
        let stepCardnumber = 1;
        const tablette = (width < 800 && width > 340)
          const mobile = width <= 340 
        if (tablette) {
            stepCardnumber = 1;
        }
        else if (mobile) {
            stepCardnumber = 1;
        }
        const diff = direction === "next" ? 1 : -1;
        const newValue = (clickCount + diff) % (items.length / stepCardnumber);
        
        setClickCount(newValue);
        scrollTo = width * newValue;

        // handleRightArrow
        if (((newValue+1)*stepCardnumber) >= items.length) {
            setHideRightArrow(true);
          } else {
            setHideRightArrow(false);
          }

        //   console.log("newValue", newValue)
        // console.log("items.length", items.length)
        // console.log("stepCardnumber", stepCardnumber)
        // console.log("scrollTo", scrollTo)
        // console.log("RESULT", ((newValue+1)*stepCardnumber))
        // console.log("*******************************")


        containerRef.current?.scrollTo({
            behavior: "smooth",
            left: scrollTo
        });
    }
  return (
    <>
        <div ref={mainWrap}>
      
      <div className="Slider_loveInvestors carousel-container-slide_loveInvestors" >
        
        <div className="carousel-inner" ref={containerRef}>
            <div className="track">
                {items?.map((item, index) => (
                <ItemComponent key={index}  
                one_title={item.one_title} 
                one_content={item.one_content} 
                one_img={item.one_img}
                 />
                ))}
            </div>
        </div>
        <div className="nav-slide_loveInvestors">
            <button className={`prev ${clickCount>0 ? "show":""}`} onClick={() => scrollSllides("prev")}>
                <i class="material-icons">arrow_left</i>
            </button>
            <button className={`next ${hideRightArrow ? "hide":""} `} onClick={() => scrollSllides("next")}>
                <i class="material-icons">arrow_right</i>
            </button>         
        </div>
      </div>
    </div>
    </>
  );
}

export default Slider;