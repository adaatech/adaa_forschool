import React, { useEffect } from "react";

import bg_picture_1 from "../../../../imgs/adaaFormateur/icon1.png";
import bg_picture_2 from "../../../../imgs/adaaFormateur/icon2.png";
import bg_picture_3 from "../../../../imgs/adaaFormateur/icon3.png";
import bg_picture_4 from "../../../../imgs/adaaFormateur/icon4.png";
import bg_picture_5 from "../../../../imgs/adaaFormateur/icon5.png";
// import bg_picture_2 from "../../../../imgs/adaaForSchool/icon5.png";

import "./style_sliderRaison.css";

const items = [
    { 
      one_title: "CONSTRUISONS ENSEMBLE LE CHAMPION AFRICAIN DU E-LEARNING", 
      one_content: "Nous avons developpé une infrastructure propriétaire et robuste, avons connecté et recruté des apprenants dans 12 pays africains déjà. Allons plus loin ensemble.", 
      one_img: bg_picture_1,
      color: "black"
    },
    { 
      one_title: "DÉJÀ 03 ANS QUE NOUS EXISTONS", 
      one_content: "Pendant ces 03 belles années, nous avons réussi de nombreux challenges, essuyé de nombreux échecs et appris de nombreuses leçons clés vitales pour asseoir un leadership africain.",  
      one_img: bg_picture_2,
      color: "var(--primaryAdaaColor)"
    }, 
    { 
      one_title: "SEULS, NOUS NE POUVONS PAS PORTER NOS IMPORTANTES AMBITIONS", 
      one_content: ["Nous visons en 3 ans",<br/>," 9 000 000 de clients Adaa Learning et d’implanter 1 salle de formation Adaa Académie dans 10 grandes métropoles africaines. Seuls on y arrivera probablement, mais ensemble on y arrivera certainement"],  
      one_img: bg_picture_3,
      color: "#c45a11"
    },
    { 
      one_title: "UN TAUX DE CROISSANCE MOYEN DE 120% SUR 3 ANS", 
      one_content: "Depuis 2019 et 2021 le nombre de nos clients a augmenté de plus 600%, notre chiffre d’affaires de plus 120%.", 
      one_img: bg_picture_4,
      color: "var(--primaryAdaaColor)"
    },
    { 
      one_title: "UNE BONNE MAITRISE DU MARCHE AFRICAIN DE LA FORMATION", 
      one_content: "Nos formations exécutées en présentielles sur 4 pays ainsi que nos offres de formations en e-learning sur 12 pays africains nous ont permis de collecter des volumes importants de données sur les marchés et les tendances avenirs.",  
      one_img: bg_picture_1,
      color: "black"
    },     
  ];

const ItemComponent = ({ one_title, one_content, one_img, one_color, fitSlider }) => {
    // return <div className="ItemComponent">{title}</div>;
    return (
        <div className={`ItemComponent card-container-slide_raison ${fitSlider ? "fit-content-slider":""} `}>
            {/* {console.log("one_color", one_color)} */}
            <div className="card-slide_raison" style={{background: one_color}}>
                {/* <div className="img"></div> */}
                <div className="box-top-slide_raison"> 
                  <div className="perso_raison" style={{background: "url("+ one_img + ") center center no-repeat", backgroundSize: "auto 100%", backgroundPosition: "center"}}></div>
                    {/* <img className="bg_picture_card_invest " src={one_img} alt="background_picture"/> */}
                    {/* <img className="one_img" src={`../../../../imgs/adaaForInvest/notre_raison/Image1.jpg`} alt=""/> */}
                </div>
                <div className="box-bottom-slide_raison">
                    <div className="box-bottom-title-slide_raison">{one_title}</div>
                    <div className="box-bottom-content-slide_raison">{one_content}</div>
                </div>
                
            </div>
        </div>
    )
  };

const Slider = () => {
    const [clickCount, setClickCount] = React.useState(0);
    const [hideRightArrow, setHideRightArrow] = React.useState(false);
    const [fitSlider, setFitSlider] = React.useState(false);

    const mainWrap = React.useRef();
    const containerRef = React.useRef();

    useEffect(() => {
      const width = mainWrap.current?.clientWidth;
      let stepCardnumber = 4;
      const tablette = (width <= 720 && width > 640)
        const tablette_mini = (width <= 640 && width > 340)
        const mobile = width <= 340 
        if (tablette) {
            stepCardnumber = 3;
        }
        else if (tablette_mini) {
          stepCardnumber = 2;
        }
        else if (mobile) {
            stepCardnumber = 1;
        }

        console.log("",stepCardnumber)
        console.log(items.length)
      if (items.length <= stepCardnumber) {
        setHideRightArrow(true);
        setFitSlider(true);
      } else {
        setHideRightArrow(false);
        setFitSlider(false);
      }
    }, [])

    const scrollSllides = (direction) => {
        const width = mainWrap.current?.clientWidth;
        console.log("mainWrap", mainWrap.current?.offsetWidth)
        // console.log("containerRef", containerRef.current?.offsetWidth)
        let scrollTo;
        let stepCardnumber = 4;
        const tablette = (width <= 720 && width > 640)
        const tablette_mini = (width <= 640 && width > 340)
        const mobile = width <= 340 
        if (tablette) {
            stepCardnumber = 3;
        }
        else if (tablette_mini) {
          stepCardnumber = 2;
        }
        else if (mobile) {
            stepCardnumber = 1;
        }
        const diff = direction === "next" ? 1 : -1;
        const newValue = (clickCount + diff) % (items.length / stepCardnumber);
        
        setClickCount(newValue);
        scrollTo = width * newValue;

        // handleRightArrow
        if (((newValue+1)*stepCardnumber) >= items.length) {
          setHideRightArrow(true);
        } else {
          setHideRightArrow(false);
        }

        // console.log("newValue", newValue)
        // console.log("items.length", items.length)
        // console.log("stepCardnumber", stepCardnumber)
        // console.log("scrollTo", scrollTo)
        // console.log("RESULT", ((newValue+1)*stepCardnumber))
        // console.log("*******************************")

        containerRef.current?.scrollTo({
            behavior: "smooth",
            left: scrollTo
        });
    }
  return (
    <>
        <div ref={mainWrap}>
      {/* <button onClick={() => scrollSllides("prev")}>Prev</button>
      <button onClick={() => scrollSllides("next")}>Next</button> */}
      
      <div className="Slider_raison carousel-container-Formateur" >
        
        <div className="carousel-inner" ref={containerRef}>
            <div className="track">
                {items?.map((item, index) => (
                <ItemComponent 
                key={index}  
                one_title={item.one_title} 
                one_content={item.one_content} 
                one_img={item.one_img} 
                one_color={item.color}
                fitSlider={fitSlider}
                />
                ))}
            </div>
        </div>
        <div className="nav-Formateur">
            <button className={`prev ${clickCount>0 ? "show":""}`} onClick={() => scrollSllides("prev")}>
                {/* <i class="material-icons">keyboard_arrow_left</i> */}
                <i class="material-icons">arrow_left</i>
            </button>
            <button className={`next ${hideRightArrow ? "hide":""} `} onClick={() => scrollSllides("next")}>
                {/* <i class="material-icons">keyboard_arrow_right</i> */}
                <i class="material-icons">arrow_right</i>
            </button>         
        </div>
      </div>
    </div>
    </>
  );
}

export default Slider;