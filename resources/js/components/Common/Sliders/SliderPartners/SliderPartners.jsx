import React from "react";

import "./style_sliderPartners.css";

import icon_1 from './partners/gray/sabc.png';
import icon_2 from './partners/gray/gtelecom.png';
import icon_3 from './partners/gray/bgfi.png';
import icon_4 from './partners/gray/olam.png';
import icon_5 from './partners/gray/axa.png';

    const items = [
        {
            id: 1,
            name: 'Big data',
            img: icon_1
        },
        {
            id: 2,
            name: 'Gabon Telecom',
            img: icon_2
        },
        {
            id: 3,
            name: 'BGFIBa',
            img: icon_3
        },
        {
            id: 4,
            name: 'olam',
            img: icon_4
        },
        {
            id: 5,
            name: 'axa',
            img: icon_5
        }
    ]

const ItemComponent = ({ partner_icon }) => {
    // return <div className="ItemComponent">{title}</div>;
    return (
        <div className="ItemComponent card_container_partners">
            
            <div className="card_partners" style={{background: "url("+ partner_icon + ") center center no-repeat", backgroundSize: "contain", backgroundPosition: "center"}}>
            {/* <img className="bg_picture_card_invest " src={bg_picture_1} alt="background_picture"/> */}
            <div className="img"></div>
            {/* <div className="info">
                Title 1
            </div> */}
            </div>
        </div>
    )
  };

const Slider = () => {
    const [clickCount, setClickCount] = React.useState(0);
    // const [respo, setRespo] = React.useState(false);
    const [hideRightArrow, setHideRightArrow] = React.useState(false);

    const mainWrap = React.useRef();
    const containerRef = React.useRef();
    
    const scrollSllides = (direction) => {
        const width = mainWrap.current?.clientWidth;
        // console.log("mainWrap", mainWrap.current?.offsetWidth)
        // console.log("containerRef", containerRef.current?.offsetWidth)
        let scrollTo;
        let stepCardnumber = 4;
        const tablette = (width <= 720 && width > 320)
        const mobile = width <= 320 
        if (tablette) {
            stepCardnumber = 2;
            
        }
        else if (mobile) {
            stepCardnumber = 1;
        }
        const diff = direction === "next" ? 1 : -1;
        const newValue = (clickCount + diff) % (items.length / stepCardnumber);
        
        setClickCount(newValue);
        scrollTo = width * newValue;

        // handleRightArrow
        if (((newValue+1)*stepCardnumber) >= items.length) {
            setHideRightArrow(true);
        } else {
            setHideRightArrow(false);
        }

        console.log("newValue", newValue)
        console.log("items.length", items.length)
        console.log("stepCardnumber", stepCardnumber)
        console.log("scrollTo", scrollTo)
        console.log("RESULT", ((newValue+1)*stepCardnumber))
        console.log("*******************************")

        containerRef.current?.scrollTo({
            behavior: "smooth",
            left: scrollTo
        });
    }
  return (
    <>
        <div ref={mainWrap}>
      {/* <button onClick={() => scrollSllides("prev")}>Prev</button>
      <button onClick={() => scrollSllides("next")}>Next</button> */}
      
      <div className="Slider carousel-container_partners" >
        <div className="nav">
            <button className={`prev ${(clickCount)>0 ? "show":""} `} onClick={() => scrollSllides("prev")}>
                <i class="material-icons">keyboard_arrow_left</i>
            </button>
            <button className={`next ${hideRightArrow ? "hide":""}`} onClick={() => scrollSllides("next")}>
                <i class="material-icons">keyboard_arrow_right</i>
            </button>         
        </div>
        <div className="carousel-inner" ref={containerRef}>
            <div className="track">
                {items?.map((item, index) => (
                <ItemComponent key={index} partner_icon={item.img} />
                ))}
            </div>
        </div>
        
      </div>
    </div>
    </>
  );
}

export default Slider;