import {React, useState, useEffect, useRef} from "react";

import bg_picture_1 from "../../../../imgs/adaaForInvest/notre_histoire/Image1.jpg";
import bg_picture_2 from "../../../../imgs/adaaForInvest/notre_histoire/Image2.jpg";

import "./style_sliderHistoire.css";

const items = [
    { 
      one_title: "Février 2019", 
      one_content: "Alors composé d’une équipe de 03 personnes, nous lançons notre 1ére formation présentielle avec location d’une salle de formation", 
      one_img: bg_picture_1
    },
    { 
      one_title: "Juillet 2019", 
      one_content: "Intégration de notre 2éme bureau (plus grand) qui faisait office de salle de formation", 
      one_img: bg_picture_2
    }, 
    { 
      one_title: "Mars 2020",
      one_content: "Alors composé d’une équipe de 03 personnes, nous lançons notre 1ére formation présentielle avec location d’une salle de formation", 
      one_img: bg_picture_1
    },
    { 
      one_title: "Avril 2020", 
      one_content: "Intégration de notre 2éme bureau (plus grand) qui faisait office de salle de formation", 
      one_img: bg_picture_2
    },
  ];

const ItemComponent = ({ one_title, one_content, one_img }) => {
    // return <div className="ItemComponent">{title}</div>;
    return (
        <div className="ItemComponent card-container-slide_histoire">
            <div className="card-slide_histoire">
                {/* <div className="img"></div> */}
                <div className="box-top-slide_histoire">
                    <div className="box-top-title-slide_histoire">{one_title}</div>
                    <div className="box-top-content-slide_histoire">{one_content}</div>
                </div>
                <div className="box-bottom-slide_histoire"> 
                <img className="bg_picture_card_invest " src={one_img} alt="background_picture"/>
                    {/* <img className="one_img" src={`../../../../imgs/adaaForInvest/notre_histoire/Image1.jpg`} alt=""/> */}
                </div>
            </div>
        </div>
    )
  };

const Slider = () => {
    const [clickCount, setClickCount] = useState(0);
    const [hideRightArrow, setHideRightArrow] = useState(false);
    const [fitSlider, setFitSlider] = useState(false);

    const mainWrap = useRef();
    const containerRef = useRef();

    useEffect(() => {
        const width = mainWrap.current?.clientWidth;
        let stepCardnumber = 2;
        const tablette = (width <= 640 && width > 340)
          const mobile = width <= 340 
          if (tablette) {
              stepCardnumber = 1;
          }
          else if (mobile) {
              stepCardnumber = 1;
          }
  
        //   console.log("",stepCardnumber)
        //   console.log(items.length)
        if (items.length <= stepCardnumber) {
          setHideRightArrow(true);
          setFitSlider(true);
        } else {
          setHideRightArrow(false);
          setFitSlider(false);
        }
      }, [])

    const scrollSllides = (direction) => {
        const width = mainWrap.current?.clientWidth;
        // console.log("mainWrap", mainWrap.current?.offsetWidth)
        // console.log("containerRef", containerRef.current?.offsetWidth)
        let scrollTo;
        let stepCardnumber = 2;
        const tablette = (width < 800 && width > 340)
          const mobile = width <= 340 
        if (tablette) {
            stepCardnumber = 1;
        }
        else if (mobile) {
            stepCardnumber = 1;
        }
        const diff = direction === "next" ? 1 : -1;
        const newValue = (clickCount + diff) % (items.length / stepCardnumber);
        
        setClickCount(newValue);
        scrollTo = width * newValue;

        // handleRightArrow
        if (((newValue+1)*stepCardnumber) >= items.length) {
            setHideRightArrow(true);
          } else {
            setHideRightArrow(false);
          }

        //   console.log("newValue", newValue)
        // console.log("items.length", items.length)
        // console.log("stepCardnumber", stepCardnumber)
        // console.log("scrollTo", scrollTo)
        // console.log("RESULT", ((newValue+1)*stepCardnumber))
        // console.log("*******************************")


        containerRef.current?.scrollTo({
            behavior: "smooth",
            left: scrollTo
        });
    }
  return (
    <>
        <div ref={mainWrap}>
      {/* <button onClick={() => scrollSllides("prev")}>Prev</button>
      <button onClick={() => scrollSllides("next")}>Next</button> */}
      
      <div className="Slider_histoire carousel-container-histoire" >
        
        <div className="carousel-inner" ref={containerRef}>
            <div className="track">
                {items?.map((item, index) => (
                <ItemComponent key={index}  
                one_title={item.one_title} 
                one_content={item.one_content} 
                one_img={item.one_img}
                 />
                ))}
            </div>
        </div>
        <div className="nav-histoire">
            <button className={`prev ${clickCount>0 ? "show":""}`} onClick={() => scrollSllides("prev")}>
                <i class="material-icons">arrow_left</i>
            </button>
            <button className={`next ${hideRightArrow ? "hide":""} `} onClick={() => scrollSllides("next")}>
                <i class="material-icons">arrow_right</i>
            </button>         
        </div>
      </div>
    </div>
    </>
  );
}

export default Slider;