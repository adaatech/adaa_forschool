import React, { useEffect } from "react";

import bg_picture_1 from "../../../../imgs/adaaAmbassador/img1.PNG";
// import bg_picture_2 from "../../../../imgs/adaaForSchool/icon5.png";

import "./style_sliderAmbassador.css";

const items = [
    { 
      name: "Koné Mamadou",
      location: "Abidjan, Cote d'ivoire",
      role: "Brand Ambassador",
      content: "J’aime vraiment travailler avec Adaa. Durant plusieurs campagnes d'abord j'ai profité de mes parcours reçus pour mieux me former et surtout j'ai dû aller vers les gens, organiser des rencontres avec des jeunes d'autres quartiers pour échanger sur les formations",
      one_img: bg_picture_1,
      
    },
    { 
      name: "Koné Mamadou",
      location: "Abidjan, Cote d'ivoire",
      role: "Brand Ambassador",
      content: "J’aime vraiment travailler avec Adaa. Durant plusieurs campagnes d'abord j'ai profité de mes parcours reçus pour mieux me former et surtout j'ai dû aller vers les gens, organiser des rencontres avec des jeunes d'autres quartiers pour échanger sur les formations",
      one_img: bg_picture_1,
    },
    { 
      name: "Koné Mamadou",
      location: "Abidjan, Cote d'ivoire",
      role: "Brand Ambassador",
      content: "J’aime vraiment travailler avec Adaa. Durant plusieurs campagnes d'abord j'ai profité de mes parcours reçus pour mieux me former et surtout j'ai dû aller vers les gens, organiser des rencontres avec des jeunes d'autres quartiers pour échanger sur les formations",
      one_img: bg_picture_1,
    },
    { 
      name: "Koné Mamadou",
      location: "Abidjan, Cote d'ivoire",
      role: "Brand Ambassador",
      content: "J’aime vraiment travailler avec Adaa. Durant plusieurs campagnes d'abord j'ai profité de mes parcours reçus pour mieux me former et surtout j'ai dû aller vers les gens, organiser des rencontres avec des jeunes d'autres quartiers pour échanger sur les formations",
      one_img: bg_picture_1,
    },
    { 
      name: "Koné Mamadou",
      location: "Abidjan, Cote d'ivoire",
      role: "Brand Ambassador",
      content: "J’aime vraiment travailler avec Adaa. Durant plusieurs campagnes d'abord j'ai profité de mes parcours reçus pour mieux me former et surtout j'ai dû aller vers les gens, organiser des rencontres avec des jeunes d'autres quartiers pour échanger sur les formations",
      one_img: bg_picture_1,
    },
     
  ];

const ItemComponent = ({ name, location, role, content, one_img, nb_participants, nb_cours, fitSlider }) => {
    // return <div className="ItemComponent">{title}</div>;
    return (
        <div className={`ItemComponent card-container-slide_ambassador ${fitSlider ? "fit-content-slider":""} `}>
            <div className="card-slide_ambassador">
                {/* <div className="img"></div> */}
                <div className="box-top-slide_ambassador"> 
                  <div className="perso_ambassador" style={{background: "url("+ one_img + ") center center no-repeat", backgroundSize: "cover", backgroundPosition: "center"}}></div>
                    {/* <img className="bg_picture_card_invest " src={one_img} alt="background_picture"/> */}
                    {/* <img className="one_img" src={`../../../../imgs/adaaForInvest/notre_ambassador/Image1.jpg`} alt=""/> */}
                </div>
                <div className="box-bottom-slide_ambassador">                    
                    <div className="box-bottom-content-slide_ambassador">"{content}"</div>
                    <div className="box-bottom-title-slide_ambassador">
                      <p>
                      <span>{name}</span>
                      <br/>{location}
                      <br/>{role}
                      </p>
                    
                    </div>
                </div>

                
            </div>
        </div>
    )
  };

const Slider = () => {
    const [clickCount, setClickCount] = React.useState(0);
    const [hideRightArrow, setHideRightArrow] = React.useState(false);
    const [fitSlider, setFitSlider] = React.useState(false);

    const mainWrap = React.useRef();
    const containerRef = React.useRef();

    useEffect(() => {
      const width = mainWrap.current?.clientWidth;
      let stepCardnumber = 3;
      const tablette = (width <= 720 && width > 340)
        // const tablette_mini = (width <= 640 && width > 340)
        const mobile = width <= 340 
        if (tablette) {
            stepCardnumber = 2;
        }
        // else if (tablette_mini) {
        //   stepCardnumber = 2;
        // }
        else if (mobile) {
            stepCardnumber = 1;
        }

        console.log("",stepCardnumber)
        console.log(items.length)
      if (items.length <= stepCardnumber) {
        setHideRightArrow(true);
        setFitSlider(true);
      } else {
        setHideRightArrow(false);
        setFitSlider(false);
      }
    }, [])

    const scrollSllides = (direction) => {
        const width = mainWrap.current?.clientWidth;
        console.log("mainWrap", mainWrap.current?.offsetWidth)
        // console.log("containerRef", containerRef.current?.offsetWidth)
        let scrollTo;
        let stepCardnumber = 3;
        const tablette = (width <= 720 && width > 340)
        // const tablette_mini = (width <= 640 && width > 340)
        const mobile = width <= 340 
        if (tablette) {
            stepCardnumber = 2;
        }
        // else if (tablette_mini) {
        //   stepCardnumber = 2;
        // }
        else if (mobile) {
            stepCardnumber = 1;
        }
        const diff = direction === "next" ? 1 : -1;
        const newValue = (clickCount + diff) % (items.length / stepCardnumber);
        
        setClickCount(newValue);
        scrollTo = width * newValue;

        // handleRightArrow
        if (((newValue+1)*stepCardnumber) >= items.length) {
          setHideRightArrow(true);
        } else {
          setHideRightArrow(false);
        }

        console.log("newValue", newValue)
        console.log("items.length", items.length)
        console.log("stepCardnumber", stepCardnumber)
        console.log("scrollTo", scrollTo)
        console.log("RESULT", ((newValue+1)*stepCardnumber))
        console.log("*******************************")

        containerRef.current?.scrollTo({
            behavior: "smooth",
            left: scrollTo
        });
    }
  return (
    <>
        <div ref={mainWrap}>
      {/* <button onClick={() => scrollSllides("prev")}>Prev</button>
      <button onClick={() => scrollSllides("next")}>Next</button> */}
      
      <div className="Slider_ambassador carousel-container-ambassador" >
        
        <div className="carousel-inner" ref={containerRef}>
            <div className="track">
                {items?.map((item, index) => (
                <ItemComponent 
                key={index}  
                one_title={item.one_title} 
                one_content={item.one_content} 
                one_img={item.one_img} 
                nb_cours={item.nb_cours}
                nb_participants={item.nb_participants}
                fitSlider={fitSlider}
                role={item.role}
                name={item.name}
                location={item.location}
                content={item.content}
                />
                ))}
            </div>
        </div>
        <div className="nav-ambassador">
            <button className={`prev ${clickCount>0 ? "show":""}`} onClick={() => scrollSllides("prev")}>
                <i class="material-icons">keyboard_arrow_left</i>
            </button>
            <button className={`next ${hideRightArrow ? "hide":""} `} onClick={() => scrollSllides("next")}>
                <i class="material-icons">keyboard_arrow_right</i>
            </button>         
        </div>
      </div>
    </div>
    </>
  );
}

export default Slider;