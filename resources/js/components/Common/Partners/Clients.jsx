import React from "react";
import Slider from 'react-slick';

// import "~slick-carousel/slick/slick.css"; 
// import "~slick-carousel/slick/slick-theme.css";

import icon_1 from './partners/gray/sabc.png';
import icon_2 from './partners/gray/gtelecom.png';
import icon_3 from './partners/gray/bgfi.png';
import icon_4 from './partners/gray/olam.png';
import icon_5 from './partners/gray/axa.png';

function Client() {

    var clients = [
        {
            id: 1,
            name: 'Big data',
            img: icon_1
        },
        {
            id: 2,
            name: 'Gabon Telecom',
            img: icon_2
        },
        {
            id: 3,
            name: 'BGFIBa',
            img: icon_3
        },
        {
            id: 4,
            name: 'olam',
            img: icon_4
        },
        {
            id: 5,
            name: 'axa',
            img: icon_5
        }
    ]

    var settings = {
        dots: true,
        infinite: true,
        speed: 1000,
        slidesToShow: clients.length >= 5 ? 5 : clients.length,
        slidesToScroll: clients.length >= 5 ? 5 : clients.length,
        autoplay: true,
        autoplaySpeed: 10000,
        nextArrow: '',
        prevArrow: '',
        responsive: [
            {
                breakpoint: 1400,
                settings: {
                  slidesToShow: clients.length >= 5 ? 5 : clients.length,
                  slidesToScroll: clients.length >= 5 ? 5 : clients.length,
                  infinite: true,
                  dots: false
                }
              },
              {
                breakpoint: 1200,
                settings: {
                  slidesToShow: clients.length >= 4 ? 4 : clients.length,
                  slidesToScroll: clients.length >= 4 ? 4 : clients.length,
                  initialSlide: 1
                }
              },
              {
                breakpoint: 900,
                settings: {
                  slidesToShow: clients.length >= 3 ? 3 : clients.length,
                  slidesToScroll: clients.length >= 3 ? 3 : clients.length
                }
              },
              {
                breakpoint: 650,
                settings: {
                  slidesToShow: clients.length >= 2 ? 2 : clients.length,
                  slidesToScroll: clients.length >= 2 ? 2 : clients.length
                }
              },
        ]
      };

    return (
            <div className="adlite-container">
                <h2 className="text-center mb-4"><b> Nos clients entreprises : </b> Ils sont satisfaits par nos services</h2>
                    <Slider {...settings}>
                        {
                            clients.map(client => (<div key={client.id}>
                            <div className="d-flex justify-content-center"><img src={client.img} alt={client.name} className="icon-clients image-fluid" /></div>
                        </div>))
                        }
                    </Slider>
            </div>
    );
}

export default Client