import {React} from "react";
import {Link} from "react-router-dom";


import "./style_secondaryBtn.css";

function SecondaryBtn({text, link, submit}) {
  return (
    
    submit ?
        <input className="oneSecondaryBtn" type="submit" value={text}/>
    :
        <Link className="oneSecondaryBtn" to={link}>
            <span>{text}</span>
        </Link>
        
    
  );
}

export default SecondaryBtn;