import React from "react";

import SectionOne from "./Section01/sectionOneAmbassador";
import SectionTwo from "./Section02/sectionTwoAmbassador";
import SectionThree from "./Section03/sectionThreeAmbassador";
import TimelineAmbassador from "../Common/Timelines/TimelineAmbassador/Timeline";

function AdaaAmbassador() {
  return (
      <>
    <SectionOne/>
    <SectionTwo/>
    <SectionThree/>
    <TimelineAmbassador/>
    </>
  );
}

export default AdaaAmbassador;