import React from "react";

import SliderAmbassador from "../../Common/Sliders/SliderAmbassador/SliderAmbassador";
import PrimaryBtn from "../../Common/PrimaryBtn/primaryBtn";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionThreeAmbassador.css";
import bg_picture from "../../../imgs/adaaForSchool/Image11.jpg";


const SectionTwo = () => {
  return (
    <>
        <section className="section03_ambassador">
            {/* <img className="bg_picture_s3_ambassador " src={bg_picture} alt="background_picture"/> */}
            <div className="bg_color_layer_s3_ambassador"></div>

            <div className="section03_ambassador_title">
                <span className="section03_ambassador_title_text_big">UNE COMMUNAUTE DE +1500 TALENTS A TRAVERS LE CONTNENT</span>
                <span className="section03_ambassador_title_text_small">Déjà +1500 Brand Ambassadors sur 14 pays africains. Rejoignez le mouvement.</span>
            </div>

            <SliderAmbassador/>

            <div className="s3_ambassador_Btn_container">
                <PrimaryBtn text={"Deviens Ambassadeur"} link={"#"}/>
            </div>
        </section>
    </>
  );
}

export default SectionTwo;