import React from "react";

import OneGridItem from "./OneGridItemS2/oneGridItem";
import Sdata from '../../../sdata/Sdata';

import "./style_sectionTwoAmbassador.css";
import bg_picture from "../../../imgs/adaaAmbassador/Image12.png";
import icon_1 from "../../../imgs/adaaAmbassador/Image7.png";
import icon_2 from "../../../imgs/adaaAmbassador/Image8.png";
import icon_3 from "../../../imgs/adaaAmbassador/Image9.png";
import icon_4 from "../../../imgs/adaaAmbassador/Image10.png";
import icon_5 from "../../../imgs/adaaAmbassador/Image11.png";

const items = [
    {
        id: 1,
        content: 'Travaillez d’où, quand et comme vous voulez.',
        imgsrc: icon_1
    },
    {
        id: 2,
        content: 'Recevez chaque 05 du mois vos revenus sur votre compte mobile money ou virement bancaire',
        imgsrc: icon_2
    },
    {
        id: 3,
        content: 'Participez gratuitement aux activités du Club Adaa de votre pays',
        imgsrc: icon_3
    },
    {
        id: 4,
        content: 'Faites commander les formations Adaa en utilisant votre CODE PROMO Ambassadeur',
        imgsrc: icon_4
    },
    {
        id: 5,
        content: 'Profitez de formations gratuites Adaa Learning suivant vos résultats',
        imgsrc: icon_5
    },
]
const SectionTwoAmbassador = () => {
  return (
    <>
        <section className="section02_ambassador">
            <img className="bg_picture_s2 " src={bg_picture} alt="background_picture"/>
            <div className="bg_color_layer_s2"></div>

            <div className="section02_ambassador_description">
                <span className="section02_ambassador_description_title">LE BRAND AMBASSADOR ADAA</span>
                <span className="section02_ambassador_description_content">
                Est toute personne (de 18 ans +) engagée  à promouvoir Adaa dans sa ville, région et pays via les outils digitaux, de bénéficier de formations gratuites chaque année et de gagner 10 à 30% de commissions sur chaque commande faite par une des personnes recommandées à Adaa par ses soins.
                </span>
            </div>

            <div className="containerBottom_s2_ambassador">
                {
                    items.map((val, ind) => {
                        return <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_content={val.content}
                                />
                    })
                }
            </div>
            {/* <div className="section02_ambassador_subtitle">
                <span>pour vous apporter l’infrastructure technologique e-learning ainsi qu’un catalogue large et riche de formations</span>
            </div> */}
            
            {/* <div class="grid-container-section02_ambassador ">
                {

                    Sdata.adaaForSchoolS2Grid.map((val, ind) => {
                        return <OneGridItem
                                key={ind}
                                one_id={val.id}
                                one_img={val.imgsrc}
                                one_title={val.title}
                                one_content={val.content}
                                />
                    })
                }

            </div> */}
        </section>
    </>
  );
}

export default SectionTwoAmbassador;