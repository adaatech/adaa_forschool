import React from "react";

import "./style_oneGridItem.css";


const OneGridItem = ({one_id, one_img, one_content}) => {
  return (
    <>
    <div class="item">
        <div className="box-top-s2_ambassador">
            <img className="one_img" src={one_img} alt=""/>
        </div>
        <div className="box-bottom-s2_ambassador">
            <div className="box-bottom-content-s2_ambassador">{one_content}</div>
        </div>
    </div>
    </>
  );
}

export default OneGridItem;