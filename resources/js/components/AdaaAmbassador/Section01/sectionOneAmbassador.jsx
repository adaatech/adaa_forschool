import {React} from "react";
import {Link} from "react-router-dom";

import SecondaryBtn from "../../Common/SecondaryBtn/secondaryBtn";
import "./style_sectionOneAmbassador.css";

import bg_picture from "../../../imgs/adaaAmbassador/Image13.jpg";
import one_img from "../../../imgs/adaaForSchool/icon6.png";

function SectionOneAmbassador() {
  return (
    <section className="section01_ambassador">
        <img className="bg_picture_s1_ambassador " src={bg_picture} alt="background_picture"/>
        <div className="teaser">
        <div className="container_s1_ambassador">
        <div className="box-top-s1_ambassador">
              <img className="one_img" src={one_img} alt=""/>
          </div>
          <div className="box-bottom-s1_ambassador">
            <div className="box-bottom-title-s1_ambassador">DEVIENS BRAND AMBASSADOR ADAA</div>
            <div className="box-bottom-content-s1_ambassador">Recrute des clients autour de toi, bénéficie des formations gratuites et gagne des revenus significatifs.</div>
          </div>
          <div className="s1_ambassador_Btn_container">
            <SecondaryBtn text={"Deviens Ambassadeur"} link={"#"}/>
          </div>
        </div> 
        </div>
    </section>
  );
}

export default SectionOneAmbassador;