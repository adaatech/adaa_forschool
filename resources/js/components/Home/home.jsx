import {React} from "react";
import { Link } from "react-router-dom";

import "./style_home.css"

function Home() {
  return (
    <>
        <div className="homeContainer">
            <Link to="/adaaForSchool" className="customBtn">adaaForSchool</Link>
            <Link to="/adaaForBusiness" className="customBtn">adaaForBusiness</Link>
            <Link to="/adaaForInvest" className="customBtn">adaaForInvest</Link>
            <Link to="/adaaFormateur" className="customBtn">adaaFormateur</Link>
            <Link to="/adaaAmbassador" className="customBtn">adaaAmbassador</Link>
        </div>
    </>
  );
}

export default Home;